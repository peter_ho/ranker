import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}
import java.util.{Spliterator, Spliterators, UUID}
import java.util.stream.StreamSupport

import com.leveragize.data.TimeSeriesData
import com.typesafe.scalalogging.StrictLogging
import de.bytefish.pgbulkinsert.PgBulkInsert
import de.bytefish.pgbulkinsert.util.PostgreSqlUtils

import scala.io.Source
import scala.util.Try
import scalikejdbc._
import jsr310._

import scala.collection.AbstractIterator
import com.leveragize.common.IteratorOps._
import com.leveragize.common.StopWatch
import com.leveragize.data.loader._

import scala.collection.immutable.VectorBuilder

trait DatasetKeyResolver[K] {
  def resolveDatasetKey(row: IndexedSeq[String]): Option[K]
}

trait TimeParser[T] {
  def parseTime(x: String): T
}

object TimeParser {
  implicit object LocalDateParser extends TimeParser[LocalDate] {
    override def parseTime(x: String): LocalDate = LocalDate.parse(x)
  }

  implicit object LocalDateTimeParser extends TimeParser[LocalDateTime] {
    override def parseTime(x: String): LocalDateTime = LocalDateTime.parse(x)
  }
}

trait ValueParser[V] {
  def parseValue(x: String): V
}

object ValueParser {
  implicit object DoubleParser extends ValueParser[Double] {
    override def parseValue(x: String): Double = {
      if (x.isEmpty) {
        Double.NaN
      }
      else {
        try {
          x.toDouble
        }
        catch {
          case _: NumberFormatException => Double.NaN
        }
      }
    }
  }

  implicit object BigDecimalParser extends ValueParser[BigDecimal] {
    override def parseValue(x: String): BigDecimal = BigDecimal(x)
  }
}

trait ColumnNameCanonicalizer {
  def canonicalize(s: String): String
}

object ColumnNameCanonicalizer {
  implicit object DefaultColumnNameCanonicalizer extends ColumnNameCanonicalizer {
    def canonicalize(s: String): String = s.withFilter(_.isLetterOrDigit).map(_.toUpper).trim
  }
}

trait ColumnNameMappings {
  def valueColumnNames: Seq[String]
  def timeColumnName: String

  implicit object DefaultColumnNameCanonicalizer extends ColumnNameCanonicalizer {
    def canonicalize(s: String): String = s.withFilter(_.isLetterOrDigit).map(_.toUpper).trim
  }

  def getMappedIndices(header: IndexedSeq[String])(implicit canonicalizer: ColumnNameCanonicalizer): IndexedSeq[Int] = {
    val headerValues = header.map(canonicalizer.canonicalize)
    val columnNames = valueColumnNames.map(canonicalizer.canonicalize)
    val arr = columnNames.map(s => headerValues.indexOf(s))

    require(arr.forall(_ >= 0), "Cannot find column")

    arr.toIndexedSeq
  }

  def timeColumnIndex(header: IndexedSeq[String])(implicit canonicalizer: ColumnNameCanonicalizer): Int = {
    val headerValues = header.map(canonicalizer.canonicalize)
    val index = headerValues.indexOf(canonicalizer.canonicalize(timeColumnName))

    require(index >= 0, "Cannot find time column")

    index
  }
}


case class ReaderSettings[T](dateTimeFilter: T => Boolean = (_: T) => true)

trait FastCSVLineReader {
  def readLine(line: String): IndexedSeq[String] = {
    val builder = new VectorBuilder[String]
    var start = 0
    var end = 0

    while ({ end = line.indexOf(',', start); end } >= 0) {
      builder += line.substring(start, end).trim
      start = end + 1
    }

    builder += line.substring(start, line.length).trim
    builder.result()
  }
}

trait TimeSeriesCSVReader[K, T, V] extends FastCSVLineReader {
  self: DatasetKeyResolver[K] with ColumnNameMappings =>

  def using[R](src: Source)(block: Source => R): Unit = {
    try {
      block(src)
    }
    finally {
      src.close()
    }
  }

  def hasFileHeader: Boolean
  def fallbackHeader: Option[IndexedSeq[String]]

  def consumeHeader(lines: Iterator[String]): IndexedSeq[String] = {
    val headerOption = if (hasFileHeader) lines.nextOption().map(readLine) else fallbackHeader

    headerOption.getOrElse(throw new IllegalArgumentException)
  }

  def read(src: Source)(dateTimeFilter: T => Boolean)
          (implicit timeParser: TimeParser[T], valueParser: ValueParser[V]): Iterator[TimeSeriesData[K, T, V]] = {

    val lines = src.getLines()
    val header = consumeHeader(lines)
    val valueIndices = getMappedIndices(header)
    val timeIndex = timeColumnIndex(header)

    def newObject(datasetKey: K, row: IndexedSeq[String]) = {
      Try(
        TimeSeriesData[K, T, V](
          datasetKey, timeParser.parseTime(row(timeIndex)),
          valueIndices.map(i => valueParser.parseValue(row(i))))
      ).toOption
    }

    for {
      ln <- lines
      row = readLine(ln)
      datasetKey <- resolveDatasetKey(row)
      obj <- newObject(datasetKey, row) if dateTimeFilter(obj.time)
    } yield obj
  }
}

trait DatasetKeyStore[C, K] {
  def getDatasetKeyByCode(code: C): Option[K]
  def listDatasetKeys: Seq[(C, K)]
}

trait PreloadedDatasetKeyResolver[C, K] extends DatasetKeyResolver[K] { self: DatasetKeyStore[C, K] =>
  private lazy val map = listDatasetKeys.toMap

  override def resolveDatasetKey(row: IndexedSeq[String]): Option[K] = map lift getDatasetCode(row)

  def getDatasetCode(row: IndexedSeq[String]): C
}

trait PgDatasetKeyStore extends DatasetKeyStore[(String, String), UUID] with StrictLogging {
  def dataType: DataType

  private [this] def joinField = dataType match {
    case PriceData => sqls"price_dataset_id"
    case VolatilityData => sqls"vol_dataset_id"
  }

  override def getDatasetKeyByCode(code: (String, String)): Option[UUID] = {
    val (databaseCode, datasetCode) = code

    val key = DB readOnly { implicit session =>
      sql"""
         |SELECT s.id
         |FROM sym_table s INNER JOIN dataset_table d ON d.id = s.$joinField
         |WHERE d.database_code = $databaseCode AND d.dataset_code = $datasetCode
        """
        .stripMargin
        .map(rs => UUID fromString rs.string(1)).single().apply()
    }

    logger.info(s"$databaseCode/$datasetCode => ${key.getOrElse("()")}")

    key
  }

  override def listDatasetKeys: Seq[((String, String), UUID)] = DB readOnly { implicit session =>
    sql"""
       |SELECT d.database_code, d.dataset_code, s.id
       |FROM sym_table s INNER JOIN dataset_table d ON d.id = s.$joinField
      """
      .stripMargin
      .map(rs => ((rs.string(1), rs.string(2)), UUID fromString rs.string(3))).list().apply()
  }
}

class LoggingIterator[+E](iterator: Iterator[E], interval: Int = 10000)
  extends AbstractIterator[E] with StrictLogging {

  require(interval > 0, "[interval] can only be a positive number")

  private var count = 0

  override def hasNext: Boolean = iterator.hasNext

  override def next(): E = {
    val next = iterator.next()

    count += 1
    if (count % interval == 0) logger.info(s"Elements processed: $count")
    next
  }
}





object DateRange {
  private def date(s: String): Option[LocalDate] = Some(s).map(_.trim).filterNot(_.isEmpty).map(LocalDate.parse)

  def from(date: String): DateRange = DateRange(Some(LocalDate parse date), None)
  def to(date: String): DateRange = DateRange(None, Some(LocalDate parse date))
}

case class DateRange(from: Option[LocalDate] = None, to: Option[LocalDate] = None) {
  def contains(date: LocalDate): Boolean = Seq(from.map(date.isBefore(_)), to.map(date.isAfter(_))).flatten.forall(!_)
}

abstract class PgLoader(databaseDefinition: DatabaseDefinition)
  extends PgBulkInsert[TimeSeriesData[UUID, LocalDate, Double]](databaseDefinition.schemaName, databaseDefinition.tableName)
    with TimeSeriesCSVReader[UUID, LocalDate, Double] with PgDatasetKeyStore with LoadFromFile
    with ColumnNameMappings with PreloadedDatasetKeyResolver[(String, String), UUID] {

  override def timeColumnName: String = databaseDefinition.timeMapping._1
  override def valueColumnNames: Seq[String] = databaseDefinition.valueMappings.map(_._1)
  override def dataType: DataType = databaseDefinition.dataType

  def save(iterator: Iterator[TimeSeriesData[UUID, LocalDate, Double]]): Unit = {
    import collection.JavaConverters._

    DB autoCommitWithConnection { conn =>
      val stream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator.asJava, Spliterator.ORDERED), false)

      saveAll(PostgreSqlUtils.getPGConnection(conn), stream)
    }
  }

  def fromSource(src: Source, dateRange: DateRange = DateRange()): Unit = {
    save(new LoggingIterator(read(src)(dateRange.contains)))
  }

  mapUUID(databaseDefinition.datasetKey, _.datasetKey)
  mapDate(databaseDefinition.timeMapping._2, _.time)

  databaseDefinition.valueMappings.zipWithIndex.foreach { case ((_, f), i) =>
    mapDouble(f, { t =>
      val d = t.values(i)

      if (d.isNaN) null else d
    })
  }
}

trait LoadDatasetFromQuandl { self: LoadOneDataset =>
  def fromQuandl(apiKey: String, dateRange: DateRange = DateRange()): Unit = {
    import DateTimeFormatter.{ISO_DATE => DF}

    val db = databaseDefinition.databaseCode
    val ds = datasetCode

    val source = Source.fromURL(s"https://www.quandl.com/api/v3/datasets/$db/$ds.csv?api_key=${apiKey.trim}" +
      dateRange.from.fold("")("&start_date=" + _.format(DF)) + dateRange.to.fold("")("&end_date=" + _.format(DF)))

    using(source)(fromSource(_, dateRange))
  }
}

trait LoadFromFile { self: PgLoader =>
  def fromFile(filename: String, dateRange: DateRange = DateRange()): Unit = {
    using(Source.fromFile(filename))(fromSource(_, dateRange))
  }
}

case class LoadOneDataset(databaseDefinition: DatabaseDefinition, datasetCode: String)
  extends PgLoader(databaseDefinition) with LoadDatasetFromQuandl {

  override def getDatasetCode(row: IndexedSeq[String]): (String, String) = (databaseDefinition.databaseCode, datasetCode)
  override def hasFileHeader: Boolean = true
  override def fallbackHeader: Option[IndexedSeq[String]] = None
}

case class LoadAllDatasets(databaseDefinition: DatabaseDefinition) extends PgLoader(databaseDefinition) {
  override def getDatasetCode(row: IndexedSeq[String]): (String, String) = (databaseDefinition.databaseCode, row(0))
  override def hasFileHeader: Boolean = false
  override def fallbackHeader: Option[IndexedSeq[String]] = Some(("Code" +: databaseDefinition.header).toIndexedSeq)
}

trait LoadData {
  type Row = (Option[LocalDate], Option[Double], Option[Double])

  def getData(symbol: String): Seq[Row] = StopWatch.printElapsedTime("LoadData") {
    DB readOnly { implicit session =>
      sql"""
         |SELECT p.date, close, 0.0
         |FROM sym_table t
         |INNER JOIN price_data p ON t.price_dataset_id = p.dataset_id
         |WHERE t.symbol = $symbol
      """
        .stripMargin
        .map(rs => (rs.localDateOpt(1), rs.doubleOpt(2), rs.doubleOpt(3))).list().apply()
    }
  }
}

object Test extends App {
  scalikejdbc.config.DBs.setupAll()

  //val apiKey = sys.env("API_KEY")

  val eodDatabase = DatabaseDefinition(
    dataType = PriceData,
    databaseCode = "EOD",
    schemaName = "public",
    tableName = "eod_input",
    datasetKey = "sym_id",
    timeMapping = "Date" -> "date",
    valueMappings = Seq("adjOpen" -> "open", "adjHigh" -> "high", "adjLow" -> "low", "adjClose" -> "close", "adjVolume" -> "volume"),
    header = TableHeader.of("Date", "Open" , "High" , "Low", "Close", "Volume", "Dividend", "Split",
      "Adj_Open", "Adj_High", "Adj_Low", "Adj_Close", "Adj_Volume")
  )

  val volDatabase = DatabaseDefinition(
    dataType = VolatilityData,
    databaseCode = "VOL",
    schemaName = "public",
    tableName = "vol_input",
    datasetKey = "sym_id",
    timeMapping = "Date" -> "date",
    valueMappings = Seq(
      "iv10c" -> "iv10c", "iv10p" -> "iv10p", "iv30c" -> "iv30c", "iv30p" -> "iv30p",
      "iv90c" -> "iv90c", "iv90p" -> "iv90p", "skew30" -> "skew30"),
    header = TableHeader(0 -> "date", 17 -> "iv10c", 18 -> "iv10p",
      25 -> "iv30c", 26 -> "iv30p", 28 -> "skew30", 33 -> "iv90c", 34 -> "iv90p")
  )

  //    Hello.printElapsedTime {
//      LoadOneDataset(eodDatabase, "MSFT").fromQuandl(apiKey, startDate = Some(LocalDate.of(2010, 1, 1)))
//      LoadOneDataset(eodDatabase, "AAPL").fromQuandl(apiKey, startDate = Some(LocalDate.of(2010, 1, 1)))
//      LoadOneDataset(eodDatabase, "XOM").fromQuandl(apiKey, startDate = Some(LocalDate.of(2010, 1, 1)))
//    }

  StopWatch.printElapsedTime("ALL") {
    LoadAllDatasets(volDatabase).fromFile("/Users/peterho/Downloads/VOL.csv", DateRange from "2002-01-01")
    LoadAllDatasets(eodDatabase).fromFile("/Users/peterho/Downloads/EOD.csv", DateRange from "2002-01-01")
//        LoadAllDatasets(volDatabase).fromFile("/Users/peterho/Downloads/VOL-partial.csv", DateRange from "2002-01-01")
//        LoadAllDatasets(eodDatabase).fromFile("/Users/peterho/Downloads/EOD-partial.csv", DateRange from "2002-01-01")
  }

}