package com.leveragize.ranker

import java.time.LocalDate
import java.util.UUID

import com.leveragize.common.StopWatch
import com.leveragize.data.DailyIndicators._
import com.leveragize.data.sql.TimeSeriesQuery
import com.leveragize.data.{ChainedCalculations, Quantiles, RangeCondition}
import com.leveragize.data.sql._
import scalikejdbc._
import jsr310._
import org.apache.commons.math3.distribution.NormalDistribution

/**
  * Created by peterho on 2/3/17.
  */
object Test extends App {
  scalikejdbc.config.DBs.setupAll()

  val readQuery =
    TimeSeriesQuery[UUID, LocalDate, Double](sqls"public", sqls"daily_data", sqls"sym_id", sqls"date",
      "open" -> sqls"open", "high" -> sqls"high", "low" -> sqls"low", "close" -> sqls"close",
      "volume" -> sqls"volume", "iv10" -> sqls"iv10", "iv30" -> sqls"iv30", "iv90" -> sqls"iv90", "skew30" -> sqls"skew30")

  val saveQuery =
    TimeSeriesQuery[UUID, LocalDate, Double](sqls"public", sqls"daily_indicator", sqls"sym_id", sqls"date",
      "close" -> sqls"close", "pctChg" -> sqls"pct_chg", "avgVol" -> sqls"avg_vol",
      "iv10" -> sqls"iv10", "iv30" -> sqls"iv30", "iv90" -> sqls"iv90", "skew30" -> sqls"skew30",
      "retT" -> sqls"ret_t", "nightRetT" -> sqls"night_ret_t", "dayRetT" -> sqls"day_ret_t",
      "dayAlpha" -> sqls"day_alpha", "dayBeta" -> sqls"day_beta",
      "vrp10Rank" -> sqls"vrp10_rank", "vrp30Rank" -> sqls"vrp30_rank", "skewZ" -> sqls"skew_z",
      "iv10Rank" -> sqls"iv10_rank", "iv30Rank" -> sqls"iv30_rank", "iv90Rank" -> sqls"iv90_rank")

  val uuids = Seq("00057356-369d-4b28-a3fd-0edf4ea84629", "0024ad6c-d786-468d-8a2f-9c3da1f260c9",
    //"003b48da-2277-496b-8d83-666079ccab55",
    "d964b252-bf7c-4f82-a040-c695f346f6ed",
    "0048bf1a-ea07-4977-bffc-a18df31f4276") map UUID.fromString

  val datasets = StopWatch.printElapsedTime("Load") {
    DB readOnly { implicit session =>
      readQuery.getDatasets(uuids, RangeCondition[T]()).values
    }
  }

  val resultDatasets = StopWatch.printElapsedTime("Process") {
    datasets map { ds =>
      println(ds.datasetKey)

      val close = ds |@ "close"
      val prevClose = close | Lag(1) |-> "prevClose"

      val retOC = ds | function("close", "open")( (cl, op) => math.log(cl / op)) |-> "retOC"
      val retCC = ds | ROC(1) |-> "retCC"
      val retCO = ds ++ prevClose | function("open", "prevClose")((op, cl) => math.log(op / cl)) |-> "retCO"

      val pctChg = retCC | function("roc")(math.exp(_) - 1.0) |-> "pctChg"
      val avgVol = ds | function("high", "low", "close", "volume") { (hi, lo, cl, vol) => (hi + lo + cl) / 3.0 * vol } | SMA(21) |-> "avgVol"

      val iv10 = ds | ForwardFill("iv10")
      val iv30 = ds | ForwardFill("iv30")
      val iv90 = ds | ForwardFill("iv90")
      val skew30 = ds | function("skew30")(x => if (x.isNaN) 0.0 else x) |-> "skew30"

      val iv10Mask = ds | ValidityMask("iv10", 252, 3, 0.05)
      val iv30Mask = ds | ValidityMask("iv30", 252, 3, 0.05)
      val iv90Mask = ds | ValidityMask("iv90", 252, 3, 0.05)
      val skew30Mask = ds | ValidityMask("skew30", 756, 3, 0.1)

      val retT = retCC | RunTTest1(21) |* "tValue" -> "retT"
      val nightRetT = retCO | RunTTest1(21) |* "tValue" -> "nightRetT"
      val dayRetT = retOC | RunTTest1(21) |* "tValue" -> "dayRetT"

      val lm = retOC ++ retCC | RunLM("retOC", "retCC", 252)
      val dayAlpha = lm |* "alpha" -> "dayAlpha"
      val dayBeta = lm |* "beta" -> "dayBeta"

      val hv = ds | VolatilityCC(1, 21) | function("hv")(_ * math.sqrt(252)) |-> "hv"
      val iv10Smoothed = iv10 | SMA(3) |-> "iv10Smoothed"
      val iv30Smoothed = iv30 | SMA(3) |-> "iv30Smoothed"

      val vrp10Rank = hv ++ iv10Smoothed | function("iv10Smoothed", "hv")(_ / _) | PercentRank(252) | ApplyMask(iv10Mask) |-> "vrp10Rank"
      val vrp30Rank = hv ++ iv30Smoothed | function("iv30Smoothed", "hv")(_ / _) | PercentRank(252) | ApplyMask(iv30Mask) |-> "vrp30Rank"

      val ivRatio30 = iv30 ++ iv90 | function("iv90", "iv30")(_ / _) |-> "ivRatio30"
      val ivRatio10 = iv10 ++ iv30 | function("iv30", "iv10")(_ / _) |-> "ivRatio10"

      val skewZ = skew30 | SMA(3) | RunZ(756) | ApplyMask(skew30Mask) |-> "skewZ"

      val iv10Rank = iv10 | PercentRank(252) | ApplyMask(iv10Mask) |-> "iv10Rank"
      val iv30Rank = iv30 | PercentRank(252) | ApplyMask(iv30Mask) |-> "iv30Rank"
      val iv90Rank = iv90 | PercentRank(252) | ApplyMask(iv90Mask) |-> "iv90Rank"

      val sigma = ds | function("iv30") { _ / math.sqrt(252) } |-> "sigma"

      val sunlight = dayAlpha ++ dayBeta | function("dayAlpha", "dayBeta") { (a, b) =>
        if (a > 0 && b > 1) {
          3
        }
        else if (a < 0 && b < 1) {
          2
        }
        else if (!a.isNaN && !b.isNaN) {
          1
        }
        else {
          0
        }
      } |-> "sunlight"


      val q = Quantiles(ds ++ sigma ++ sunlight ++ vrp10Rank ++ vrp30Rank ++ ivRatio10 ++ ivRatio30 ++ skewZ ++ iv10Rank ++ iv30Rank ++ dayRetT ++ nightRetT ++ retT, ChainedCalculations(SigmaMoves(-21), Select("ret")))
      val norm = new NormalDistribution()
      val pList = Seq(-1.0, -0.5, 0.0, 0.5, 1.0) map norm.cumulativeProbability


      println("----------")
      q.printSummary(pList)
      println("*")
      q.printSummary(pList, Select("sunlight")) { case x: Double if x > 0 => x.toString }
      println("*")
      q.printSummary(pList, Select("vrp10Rank")) { case x if x > 0.95 => "HHvrp10"; case x if x < 0.05 => "LLvrp10"  }
      println("*")
      q.printSummary(pList, Select("vrp30Rank")) { case x if x > 0.95 => "HHvrp30"; case x if x < 0.05 => "LLvrp30"  }
      println("*")
      q.printSummary(pList, Select("ivRatio10")) { case x if x > 1.2 => "HHivr10"; case x if x < 0.8 => "LLivr10"  }
      println("*")
      q.printSummary(pList, Select("ivRatio30")) { case x if x > 1.2 => "HHivr30"; case x if x < 0.8 => "LLivr30"  }
      println("*")
      q.printSummary(pList, Select("skewZ")) { case x if x > 1 => "HHskewZ"; case x if x < -1 => "LLskewZ"  }
      println("*")
      q.printSummary(pList, Select("iv10Rank")) { case x if x > 0.9 => "HH1"; case x if x < 0.1 => "LL1" }
      println("*")
      q.printSummary(pList, Select("iv30Rank")) { case x if x > 0.9 => "HH"; case x if x < 0.1 => "LL" }
      println("*")
      q.printSummary(pList, Select("nightRetT")) { case x if x > 2 => "nUU"; case x if x < -2 => "nDD"; case _ => "O"  }
      println("*")
      q.printSummary(pList, Select("dayRetT")) { case x if x > 2 => "dUU"; case x if x < -2 => "dDD"; case _ => "O"  }
      println("*")
      q.printSummary(pList, Select("retT")) { case x if x > 1 => "UU"; case x if x < -1 => "DD" }

      (close ++ pctChg ++ avgVol ++ iv10 ++ iv30 ++ iv90 ++ skew30 ++ retT ++ nightRetT ++ dayRetT ++ dayAlpha ++ dayBeta ++
        vrp10Rank ++ vrp30Rank ++ skewZ ++ iv10Rank ++ iv30Rank ++ iv90Rank).withIndexRange(800)
    }
  }

  StopWatch.printElapsedTime("Save") {
//    DB localTx { implicit session =>
//      saveQuery.save(resultDatasets)
//    }
  }
}
