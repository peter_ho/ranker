package com.leveragize.ranker.http

import java.time.LocalDate

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import com.leveragize.data.{DailyAnalytics, DailyIndicatorSummary, SnapshotTable, SummaryTable, Symbol}
import spray.json.{DefaultJsonProtocol, JsonFormat, RootJsonFormat}

import scala.io.StdIn


case class ResponseWithProperties[R: JsonFormat](list: Seq[R], properties: Map[String, String])


/**
  * Created by peterho on 4/19/17.
  */
object Main extends App with SprayJsonSupport {
  implicit val system = ActorSystem("my-system")
  implicit val m = ActorMaterializer()
  implicit val ec = system.dispatcher

  scalikejdbc.config.DBs.setupAll()

  val route =
    get {
      path("list") {
        encodeResponse {
          import com.leveragize.data.SymbolProtocol._

          complete(Symbol.list)
        }
      } ~
      path("summary") {
        encodeResponse {
          import com.leveragize.data.SummaryTableProtocol._

          complete(SummaryTable.list)
        }
      } ~
      path("snapshot") {
        encodeResponse {
          import com.leveragize.data.SnapshotTableProtocol._

          complete(SnapshotTable.get(LocalDate.of(2017, 7, 20)))
        }
      } ~
      path("analytics" / Segment) { s =>
        encodeResponse {
          import com.leveragize.data.DailyAnalyticsProtocol._

          complete(DailyAnalytics.get(s))
        }
      } ~
      pathPrefix("web") {
        getFromDirectory("/Users/peterho/RubymineProjects/chartapp")
      }
    }


  val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
