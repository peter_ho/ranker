package com.leveragize.data.loader

import com.github.tototoshi.csv.CSVReader
import scalikejdbc._

/**
  * Created by peterho on 4/6/17.
  */
object DatasetCodeLoader {
  def process(filename: String): Unit = {
    val reader = CSVReader.open(filename)
    val code = "([^/]+)/([^/]+)".r

    try {
      DB localTx { implicit session =>
        reader
          .iterator
          .collect { case code(db, ds) +: desc +: _ =>
            val database = db.trim
            val dataset = ds.trim
            val description = desc.trim

            Seq(database, dataset, description)
          }
          .grouped(100)
          .foreach { group =>
            sql"""
                 INSERT INTO dataset_table (database_code, dataset_code, name) VALUES (?, ?, ?)
                 ON CONFLICT DO NOTHING
              """.batch(group: _*).apply()
          }
      }
    }
    finally {
      reader.close()
    }
  }
}

object LoadDatasetCodes extends App {
  scalikejdbc.config.DBs.setupAll()
  DatasetCodeLoader.process("/Users/peterho/Downloads/EOD-datasets-codes.csv")
  DatasetCodeLoader.process("/Users/peterho/Downloads/VOL-datasets-codes.csv")
}