package com.leveragize.data.loader

import java.util.UUID

import com.leveragize.data._
import scalikejdbc.DBSession


/**
  * Created by peterho on 4/18/17.
  */
class DailyDataLoader(val processId: Int, override val batchSize: Int = 10)
  extends DataLoader[DailyDataset] { dataProcessor: DailyDataProcessor =>

  def save(outputs: Map[UUID, DailyDataset])(implicit session: DBSession): Unit = {
    saveQuery.saveDatasets(outputs.values.map(_.withIndexRange(dropFirst)))
  }
}

trait DailyDataProcessor extends DataProcessor[DailyDataset] {
  def saveQuery: DailyDatasetQuery
  def dropFirst: Int = 0
}
