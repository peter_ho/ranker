package com.leveragize.data.loader

/**
  * Created by peterho on 4/9/17.
  */
case class DatabaseDefinition(dataType: DataType, databaseCode: String, schemaName: String, tableName: String,
                              datasetKey: String, timeMapping: (String, String), valueMappings: Seq[(String, String)],
                              header: TableHeader)

class TableHeader(fields: Map[Int, String]) {
  require(fields.keys.forall(_ >= 0))

  def toIndexedSeq: IndexedSeq[String] = (0 to fields.keys.max).map(fields.getOrElse(_, ""))
  def toSeq: Seq[String] = toIndexedSeq

  def +:(field: String): TableHeader = {
    val shifted = fields.map { case (i, f) => (i + 1, f) }

    new TableHeader(shifted.updated(0, field))
  }
}

object TableHeader {
  def of(fields: String*): TableHeader = new TableHeader(fields.zipWithIndex.map(_.swap).toMap)
  def apply(fields: Map[Int, String]): TableHeader = new TableHeader(fields)
  def apply(fields: (Int, String)*): TableHeader = new TableHeader(fields.toMap)
}