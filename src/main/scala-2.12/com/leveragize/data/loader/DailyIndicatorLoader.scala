package com.leveragize.data.loader

import com.leveragize.common.{Second, StopWatch}

/**
  * Created by peterho on 4/17/17.
  */
object DailyIndicatorLoader extends App {
  scalikejdbc.config.DBs.setupAll()

  val loader = new DailyDataLoader(1) with DailyIndicatorProcessor
  implicit val timeUnit = Second

  StopWatch.printElapsedTime("DailyIndicatorLoader") {
    loader.loadData()
  }
}
