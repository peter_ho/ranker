package com.leveragize.data.loader
import java.time.LocalDate
import java.util.UUID

import com.leveragize.common.{Second, StopWatch}
import com.leveragize.data.DailyIndicators._
import com.leveragize.data._
import com.leveragize.data.sql._
import com.leveragize.stat
import com.leveragize.stat.{LinearModel, TTest}
import scalikejdbc._


/**
  * Created by peterho on 6/5/17.
  */
class SummaryDataLoader(val processId: Int) extends DataLoader[Map[String, Double]] with DataProcessor[Map[String, Double]] {
  object IndicatorProcessor extends DailyIndicatorProcessor

  override def readQuery: DailyDatasetQuery = DailyIndicator.query

  override def loadTableName: scalikejdbc.SQLSyntax = sqls"summary_load_table"

  override def process(ds: DailyDataset): Map[String, Double] = {
    def lm(a1: IndexedSeq[Double], a2: IndexedSeq[Double]): LinearModel = {
      LinearModel(a1 zip a2 filterNot { case (x, y) => x.isNaN || y.isNaN })
    }

    def avg(a: Seq[Double]): Double = {
      val (sum, count) = a.foldLeft((0.0, 0)) { case ((acc, n), x) => if (x.isNaN) (acc, n) else (acc + x, n + 1) }

      if (count > 0) sum / count else Double.NaN
    }

    def div(a1: IndexedSeq[Double], a2: IndexedSeq[Double]): IndexedSeq[Double] = a1 zip a2 map { case (x, y) =>
      val t = x / y

      if (t.isInfinite) Double.NaN else t
    }

    def signChangeFrequency(a: IndexedSeq[Double]): Double = {
      val pairs = a.drop(1) zip a

      val count = a.drop(1) zip a count { case (x, y) =>
        !x.isNaN && !y.isNaN && x.signum != y.signum
      }

      count.toDouble / (pairs.length.toDouble / 252.0)
    }

    def calcEntropy(entries: IndexedSeq[(LocalDate, Double)]): Double = {
      def fd(logRets: IndexedSeq[Double]) = {
        val t = logRets.filterNot(_.isNaN).scanLeft(0.0)(_ + _).map(math.exp)
        val len = t.length
        val n = len - (1 - len % 2) // n must be odd
        val x = t.slice(0, n)
        val p = (n - 1) / 2
        val a = x.slice(0, p + 1)
        val b = x.slice(p, n)

        def range(arr: IndexedSeq[Double]) = arr.max - arr.min

        ((math.log(range(a) / p + range(b) / p) - math.log(range(x) / (2.0 * p))) / math.log(2.0)) max 1.0 min 2.0
      }


      def autoCor(r: IndexedSeq[Double]) = if (r.lengthCompare(10) > 0) stat.correlation(r, r.drop(1)) else Double.NaN

      val counts = entries
        .groupBy(t => t._1.getYear + "H" + ((t._1.getMonthValue - 1) / 6 + 1))
        .values.map(t => autoCor(t.unzip._2.filterNot(_.isNaN))).filterNot(_.isNaN)
        .groupBy(_ < 0.0)
        .map { case (_, t) => t.size }

      val n = counts.sum

      -counts.foldLeft(0.0) { (acc, x) =>
        val p = x.toDouble / n.toDouble

        acc + p * math.log(p) / math.log(2)
      }
    }

    def cumSum(x: IndexedSeq[Double]): IndexedSeq[Double] = x.filterNot(_.isNaN).scanLeft(0.0)(_ + _).drop(1)
    def lastN(n: Int)(x: IndexedSeq[Double]): IndexedSeq[Double] = x.slice(x.length - n, x.length)

    def calcRetUpDn(pairs: IndexedSeq[(Double, Double)]): Double = {
      val retUp = pairs collect { case (r, ma) if ma > 0 => r }
      val retDn = pairs collect { case (r, ma) if ma <= 0 => r }

      diffScore(TTest(retUp, retDn))
    }

    def calcCondAvg(pairs: IndexedSeq[(Double, Double)], cond: Double => Boolean, minCount: Int = 100): Double = {
      val col = pairs collect { case (r, x) if !x.isNaN && !r.isNaN && cond(x) => r }

      if (col.lengthCompare(minCount) < 0) Double.NaN else avg(col)
    }

    def diffScore(test: TTest) = {
      if (test.v > 0) {
        val t = test.t
        val p = test.pValue

        (1.0 - p) * t.signum
      }
      else {
        Double.NaN
      }
    }

    val retCC = (ds | function("retCO", "retOC")(_ + _)).firstDataColumn.values
    val retCO = ds.dataValues("retCO")
    val retOC = ds.dataValues("retOC")
    val retAvg = avg(retCC)

    val rsi10 = ds dataValues "rsi10"
    val er21 = ds dataValues "er21"
    val mov21 = ds dataValues "mov21"
    val rIv30Iv90 = (ds | function("iv30S", "iv90S")(_ / _)).dataValues
    val rIv30Hv = (ds | function("iv30S", "hv")(_ / _)).dataValues

    val iv30Rank = ds dataValues "iv30Rank"
    val hvRank = ds dataValues "hvRank"

    val sigmaFut = ds |@ "ret21" | Lead(21) |-> "sigmaFut"
    val call = (sigmaFut | function { math.max(_, 0.0) - 0.4 }).dataValues
    val put = (sigmaFut | function { -math.min(_, 0.0) - 0.4 }).dataValues

    val ivData = ds.dataValues("iv30Rank").dropWhile(_.isNaN)
    val length = ivData.length
    val nullPct = if (length > 0) ivData.count(_.isNaN).toDouble / length else 1.0

    val ma20 = ds dataValues "ma20"
    val ma50 = ds dataValues "ma50"
    val ma200 = ds dataValues "ma200"

    Map(
      "ret_co_oc" -> diffScore(TTest.paired(retCO zip retOC)),
      "ret_ma20" -> calcRetUpDn(retCC.drop(1) zip ma20),
      "ret_ma50" -> calcRetUpDn(retCC.drop(1) zip ma50),
      "ret_ma200" -> calcRetUpDn(retCC.drop(1) zip ma200),
      "call_mov21_hi" -> calcCondAvg(call zip mov21, _ > 1.0),
      "call_mov21_lo" -> calcCondAvg(call zip mov21, _ < -1.0),
      "put_mov21_hi" -> calcCondAvg(put zip mov21, _ > 1.0),
      "put_mov21_lo" -> calcCondAvg(put zip mov21, _ < -1.0),
      "call_er21_hi" -> calcCondAvg(call zip er21, _ > 0.4),
      "call_er21_lo" -> calcCondAvg(call zip er21, _ < 0.1),
      "put_er21_hi" -> calcCondAvg(put zip er21, _ > 0.4),
      "put_er21_lo" -> calcCondAvg(put zip er21, _ < 0.1),
      "call_rsi10_hi" -> calcCondAvg(call zip rsi10, _ > 75.0),
      "call_rsi10_lo" -> calcCondAvg(call zip rsi10, _ < 25.0),
      "put_rsi10_hi" -> calcCondAvg(put zip rsi10, _ > 75.0),
      "put_rsi10_lo" -> calcCondAvg(put zip rsi10, _ < 25.0),
      "call_iv30_iv90_hi" -> calcCondAvg(call zip rIv30Iv90, _ > 1.05),
      "call_iv30_iv90_lo" -> calcCondAvg(call zip rIv30Iv90, _ < 0.90),
      "put_iv30_iv90_hi" -> calcCondAvg(put zip rIv30Iv90, _ > 1.05),
      "put_iv30_iv90_lo" -> calcCondAvg(put zip rIv30Iv90, _ < 0.90),
      "call_iv30_hv_hi" -> calcCondAvg(call zip rIv30Hv, _ > 1.3),
      "call_iv30_hv_lo" -> calcCondAvg(call zip rIv30Hv, _ < 0.9),
      "put_iv30_hv_hi" -> calcCondAvg(put zip rIv30Hv, _ > 1.3),
      "put_iv30_hv_lo" -> calcCondAvg(put zip rIv30Hv, _ < 0.9),
      "call" -> avg(call), "put" -> avg(put),
      "ret_avg" -> retAvg, "length" -> length / 252.0, "null_pct" -> nullPct
    )
  }

  override def save(outputs: Map[UUID, Map[String, Double]])(implicit session: DBSession): Unit = {
    val fields = SummaryTable.dataFields.map(_._2)
    val sqlNames = SQLSyntax.join(fields, sqls",", spaceBeforeDelimier = false)
    val placeholders = SQLSyntax.join(fields.map(_ => sqls"?"), sqls",", spaceBeforeDelimier = false)
    val conflictUpdates = SQLSyntax.join(fields.map { s => sqls"$s = EXCLUDED.$s" }, sqls",", spaceBeforeDelimier = false)

    val sql =
      sql"""
         |INSERT INTO public.summary_table (sym_id, $sqlNames)
         |VALUES (?, $placeholders)
         |ON CONFLICT (sym_id) DO UPDATE SET $conflictUpdates
        """
        .stripMargin

    val pbfK = implicitly[ParameterBinderFactory[UUID]]
    val defaults = implicitly[DataDefaults[Double]]

    val batchParams = outputs.toList.collect { case (key, obj) if obj.nonEmpty =>
      pbfK(key) :: SummaryTable.dataFields.map { case (k, _) => defaults.makeOption(obj.getOrElse(k, Double.NaN)) }.toList
    }

    sql.batch(batchParams: _*).apply()
  }
}

object MySummaryDataLoader extends App {
  scalikejdbc.config.DBs.setupAll()

  val loader = new SummaryDataLoader(1)
  implicit val timeUnit = Second

  StopWatch.printElapsedTime("DailyIndicatorLoader") {
    loader.loadData()
  }
}
