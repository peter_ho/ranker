package com.leveragize.data.loader

import java.time.LocalDate
import java.util.UUID

import com.leveragize.data.{DailyDataset, DailyDatasetQuery, DailyIndicator}
import com.leveragize.data.DailyIndicators._
import com.leveragize.data.sql._
import scalikejdbc._
import jsr310._
/**
  * Created by peterho on 4/18/17.
  */
trait DailyIndicatorProcessor extends DailyDataProcessor {
  val readQuery: DailyDatasetQuery =
    TimeSeriesQuery[UUID, LocalDate, Double](sqls"public", sqls"daily_data", sqls"sym_id", sqls"date",
        "open" -> sqls"open", "high" -> sqls"high", "low" -> sqls"low", "close" -> sqls"close",
        "volume" -> sqls"volume", "iv10c" -> sqls"iv10c", "iv10p" -> sqls"iv10p",
        "iv30c" -> sqls"iv30c", "iv30p" -> sqls"iv30p", "iv90c" -> sqls"iv90c", "iv90p" -> sqls"iv90p",
        "skew30" -> sqls"skew30")

  val saveQuery: DailyDatasetQuery = DailyIndicator.query

  val loadTableName: SQLSyntax = sqls"load_table"

  override val dropFirst = 300

  override def process(ds: DailyDataset): DailyDataset = {
    val close = ds |@ "close"
    val prevClose = close | Lag(1) |-> "prevClose"

    val retOC = ds | function("close", "open")((cl, op) => math.log(cl / op)) |-> "retOC"
    val retCC = ds | ROC(1) |-> "retCC"
    val retCO = ds ++ prevClose | function("open", "prevClose")((op, cl) => math.log(op / cl)) |-> "retCO"

    val avgVol = ds | function("high", "low", "close", "volume") { (hi, lo, cl, vol) => (hi + lo + cl) / 3.0 * vol } | SMA(21) |-> "avgVol"

    def ivMean(ivCall: Double, ivPut: Double): Double = (ivCall + ivPut) / 2.0

    val iv30 = ds | function("iv30c", "iv30p")(ivMean) |-> "iv30"
    val iv90 = ds | function("iv90c", "iv90p")(ivMean) |-> "iv90"

    val iv30F = iv30 | ForwardFill("iv30") |-> "iv30F"
    val iv90F = iv90 | ForwardFill("iv90") |-> "iv90F"

    val iv30Mask = iv30 | ValidityMask("iv30", 252, 3, 0.05)
    val iv90Mask = iv90 | ValidityMask("iv90", 252, 3, 0.05)

    val night21 = retCO | RunTTest1(21) |* "tValue" -> "night21"
    val day21 = retOC | RunTTest1(21) |* "tValue" -> "day21"

    val mov21 = retCC | RunTTest1(21) |* "tValue" -> "mov21"
    val mov63 = retCC | RunTTest1(63) |* "tValue" -> "mov63"
    val mov252 = retCC | RunTTest1(252) |* "tValue" -> "mov252"

    val er21 = ds | ER(21) |-> "er21"
    val er63 = ds | ER(63) |-> "er63"

    val hv = ds | VolatilityYZ(21) |-> "hv"
    val hvRank = hv | PercentRank(252) |-> "hvRank"

    val iv30Rank = iv30F | PercentRank(252) | ApplyMask(iv30Mask) |-> "iv30Rank"
    val iv90Rank = iv90F | PercentRank(252) | ApplyMask(iv90Mask) |-> "iv90Rank"

    val iv30Stoch = iv30F |* ("iv30F" -> "high", "iv30F" -> "low", "iv30F" -> "close") | StochOsc(252) | ApplyMask(iv30Mask) |-> "iv30Stoch"
    val iv90Stoch = iv90F |* ("iv90F" -> "high", "iv90F" -> "low", "iv90F" -> "close") | StochOsc(252) | ApplyMask(iv90Mask) |-> "iv90Stoch"

    val iv30S = iv30F | SMA(3) | ApplyMask(iv30 | ValidityMask("iv30", 3, 3, 0.00)) |-> "iv30S"
    val iv90S = iv90F | SMA(3) | ApplyMask(iv90 | ValidityMask("iv90", 3, 3, 0.00)) |-> "iv90S"

    val ma20 = close | RunZ(20) |-> "ma20"
    val ma50 = close | RunZ(50) |-> "ma50"
    val ma200 = close | RunZ(200) |-> "ma200"
    val stoch63 = ds | StochOsc(63) |-> "stoch63"
    val stoch252 = ds | StochOsc(252) |-> "stoch252"

    val rsi5 = ds | RSI(5) |-> "rsi5"
    val rsi10 = ds | RSI(10) |-> "rsi10"

    val roc21 = ds | ROC(21) |-> "roc21"
    val roc252 = ds | ROC(252) |-> "roc252"

    val sigma = iv30 | Lag(21) |-> "sigma"
    val ret21 = roc21 ++ sigma | function("roc21", "sigma") { (r, s) => r / (s * math.sqrt(21.0 / 252.0)) } |-> "ret21"

    avgVol ++ roc252 ++ night21 ++ day21 ++ mov21 ++ mov63 ++ mov252 ++ iv30Rank ++ iv90Rank ++
      iv30Stoch ++ iv90Stoch ++ iv30S ++ iv90S ++ hv ++ hvRank ++ er21 ++ er63 ++ ma20 ++ ma50 ++ ma200 ++
      stoch63 ++ stoch252 ++ rsi5 ++ rsi10 ++ retCO ++ retOC ++ ret21
  }
}
