package com.leveragize.data.loader

import java.util.UUID

import com.leveragize.data.{DailyDataset, DailyDatasetQuery}
import com.leveragize.data.sql._
import scalikejdbc._

/**
  * Created by peterho on 4/18/17.
  */
trait DataLoader[T] { dataProcessor: DataProcessor[T] =>
  def batchSize: Int = 10

  def datasetKeys: Seq[UUID] = DB readOnly { implicit session =>
    sql"SELECT sym_id FROM ${dataProcessor.loadTableName} WHERE process_id = $processId".map(rs => rs.get[UUID](1)).list().apply()
  }

  def loadData(): Unit = {
    datasetKeys.grouped(batchSize).foreach { batch =>
      val outputs = DB readOnly { implicit session => read(batch) mapValues process }

      DB localTx { implicit session =>
        val pbf = implicitly[ParameterBinderFactory[UUID]]

        save(outputs)

        batch.foreach { uuid =>
          sql"UPDATE ${dataProcessor.loadTableName} SET last_updated = now(), process_id = NULL WHERE sym_id = ${pbf(uuid)}".update.apply()
        }
      }
    }
  }


  def read(batch: Seq[UUID])(implicit session: DBSession): Map[UUID, DailyDataset] = readQuery.getDatasets(batch)
  def save(outputs: Map[UUID, T])(implicit session: DBSession): Unit

  def processId: Int
}

trait DataProcessor[T] {
  def readQuery: DailyDatasetQuery
  def loadTableName: SQLSyntax
  def process(ds: DailyDataset): T
}