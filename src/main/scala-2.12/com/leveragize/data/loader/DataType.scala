package com.leveragize.data.loader

/**
  * Created by peterho on 4/9/17.
  */
trait DataType
case object PriceData extends DataType
case object VolatilityData extends DataType
