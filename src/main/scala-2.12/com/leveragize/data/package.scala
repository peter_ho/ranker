package com.leveragize

import java.time.LocalDate
import java.util.UUID

import com.leveragize.data.sql.TimeSeriesQuery

/**
  * Created by peterho on 3/21/17.
  */
package object data {
  type DailyDataset = ColumnSet[UUID, LocalDate, Double]
  type DailyCalculation = Calculation[UUID, LocalDate, Double]
  type DailyDatasetQuery = TimeSeriesQuery[UUID, LocalDate, Double]
}
