package com.leveragize.data

import java.time.{LocalDate, LocalDateTime}

import scala.collection.immutable.VectorBuilder
import scala.collection.mutable
import scala.reflect.ClassTag

/**
  * Created by peterho on 1/20/17.
  */
case class TimeSeriesDatasetMetadata(columnNames: IndexedSeq[String]) {
  def columnIndexOf(name: String): Option[Int] = {
    val index = columnNames.indexOf(name)

    if (index < 0) None else Some(index)
  }
}

case class TimeSeriesData[K, T, V](datasetKey: K, time: T, values: IndexedSeq[V])


trait Column[U] { self =>
  def length: Int
  def values: IndexedSeq[U]
  def get(index: Int): U

  def indices: Range = 0 until length

  def indexWhere(p: U => Boolean, from: Int = 0): Int = {
    val len = length
    var i = from

    while (i < len) {
      if (p(get(i))) return i
      i += 1
    }

    -1
  }

  def first: U = get(0)
  def last: U = get(length - 1)

  def firstOption: Option[U] = if (length > 0) Some(get(0)) else None

  def lastOption: Option[U] = {
    val len = length

    if (len > 0) Some(get(len - 1)) else None
  }

  override def toString: String = values.toString
}

trait TimeSeriesDataColumn[V] extends Column[V] {
  def name: String
  def rename(newName: String): TimeSeriesDataColumn[V]

  def apply(index: Int)(implicit defaults: DataDefaults[V]): V = {
    if (index < 0 || index >= length) defaults.invalidValue else get(index)
  }

  def apply(index: Int, replacement: V)(implicit defaults: DataDefaults[V]): V = {
    if (index < 0 || index >= length) replacement else defaults.replaceInvalid(get(index), replacement)
  }

  def slice(from: Int, until: Int): TimeSeriesDataColumn[V]
}

trait TimeSeriesTimeColumn[T] extends Column[T] {
  def apply(index: Int): T = get(index)
  def slice(from: Int, until: Int): TimeSeriesTimeColumn[T]
}

trait VectorColumn[U] extends Column[U] {
  def v: Vector[U]

  override def length: Int = v.length
  override def indices: Range = v.indices
  override def values: IndexedSeq[U] = v
  override def get(index: Int): U = v(index)
}

class VectorTimeColumn[T](val v: Vector[T]) extends VectorColumn[T] with TimeSeriesTimeColumn[T] {
  override def slice(from: Int, until: Int): VectorTimeColumn[T] = new VectorTimeColumn[T](v.slice(from, until))
}

class VectorDataColumn[V](val name:String, val v: Vector[V]) extends VectorColumn[V] with TimeSeriesDataColumn[V] {
  override def rename(to: String): TimeSeriesDataColumn[V] = new VectorDataColumn[V](to, v)
  override def slice(from: Int, until: Int): VectorDataColumn[V] = new VectorDataColumn[V](name, v.slice(from, until))
}

trait ColumnSetFactory[K, T, V] {
  def newColumnSet(data: Seq[(String, Vector[V])]): ColumnSet[K, T, V]
}

trait ColumnSet[K, T, V] extends ColumnSetFactory[K, T, V] { self =>
  def datasetKey: K
  def columnNames: Seq[String]
  def timeColumn: TimeSeriesTimeColumn[T]
  def dataColumnOption(columnName: String): Option[TimeSeriesDataColumn[V]]
  def dataColumnOption(columnIndex: Int): Option[TimeSeriesDataColumn[V]]

  def dataColumns(columnNames: Seq[String]): Seq[TimeSeriesDataColumn[V]] = {
    columnNames.map(dataColumn)
  }

  def select(columnNames: String*): ColumnSet[K, T, V]
  def selectFirst: ColumnSet[K, T, V]
  def selectAs(mappings: Seq[(String, String)]): ColumnSet[K, T, V]

  def dataColumn(columnName: String): TimeSeriesDataColumn[V] = {
    dataColumnOption(columnName).getOrElse(throw new NoSuchElementException(columnName))
  }

  def dataColumn(columnIndex: Int): TimeSeriesDataColumn[V] = {
    dataColumnOption(columnIndex).getOrElse(throw new NoSuchElementException(s"Index: $columnIndex"))
  }

  def allDataColumns: Seq[TimeSeriesDataColumn[V]] = columnNames.map(dataColumn)
  def timeValues: IndexedSeq[T] = timeColumn.values
  def dataValues(columnName: String): IndexedSeq[V] = dataColumn(columnName).values
  def dataValues: IndexedSeq[V] = if (columnCountCompare(1) == 0) firstDataColumn.values else throw new RuntimeException
  def lastValue(columnName: String): V = dataColumn(columnName).get(length - 1)
  def columnCount: Int = columnNames.length
  def columnCountCompare(count: Int):Int = columnNames.lengthCompare(count)
  def firstDataColumn: TimeSeriesDataColumn[V]
  def ++(that: ColumnSet[K, T, V]): ColumnSet[K, T, V]
  def | (calc: Calculation[K, T, V])(implicit ev: ClassTag[V], defaults: DataDefaults[V]): ColumnSet[K, T, V] = calc(self)
  def |++ (calc: Calculation[K, T, V])(implicit ev: ClassTag[V], defaults: DataDefaults[V]): ColumnSet[K, T, V] = self ++ calc(self)
  def |-> (columnName: String): ColumnSet[K, T, V]
  def |* (m: (String, String)*): ColumnSet[K, T, V] = selectAs(m)
  def |@ (n: String*): ColumnSet[K, T, V] = select(n: _*)

  def withRenamedColumns(map: Map[String, String]): ColumnSet[K, T, V]
  def withRenamedColumns(names: Seq[String]): ColumnSet[K, T, V]
  def withRenamedColumns(f: String => String): ColumnSet[K, T, V]

  def withIndexRange(from: Int, until: Int): ColumnSet[K, T, V]
  def withIndexRange(from: Int): ColumnSet[K, T, V] = withIndexRange(from, timeColumn.length)

  def indices: Range = timeColumn.indices
  def length: Int = timeColumn.length
}

trait ColumnSetWithSelect[K, T, V] { self: ColumnSet[K, T, V] =>
  def select(columnNames: String*): ColumnSet[K, T, V] = {
    new DefaultColumnSet(datasetKey, timeColumn, dataColumns(columnNames))
  }

  def selectFirst: ColumnSet[K, T, V] = {
    new DefaultColumnSet(datasetKey, timeColumn, Seq(firstDataColumn))
  }
}

trait ColumnSetWithNew[K, T, V] { self: ColumnSet[K, T, V] =>
  override def newColumnSet(data: Seq[(String, Vector[V])]): ColumnSet[K, T, V] = {
    val len = timeColumn.length

    val newCols = data.map { case (n, v) =>
      require(v.lengthCompare(len) == 0)
      new VectorDataColumn[V](n, v)
    }

    new DefaultColumnSet(datasetKey, timeColumn, newCols)
  }
}

trait ColumnSetWithRename[K, T, V] { self: ColumnSet[K, T, V] =>
  override def withRenamedColumns(names: Seq[String]): ColumnSet[K, T, V] = {
    new DefaultColumnSet[K, T, V](datasetKey, timeColumn, allDataColumns zip names map { case (c, n) => c.rename(n) })
  }

  override def withRenamedColumns(map: Map[String, String]): ColumnSet[K, T, V] = {
    new DefaultColumnSet[K, T, V](datasetKey, timeColumn, allDataColumns.map(c => map.lift(c.name).fold(c)(s => c.rename(s))))
  }

  override def withRenamedColumns(f: String => String): ColumnSet[K, T, V] = {
    new DefaultColumnSet[K, T, V](datasetKey, timeColumn, allDataColumns.map(c => c.rename(f(c.name))))
  }

  override def selectAs(mappings: Seq[(String, String)]): ColumnSet[K, T, V] = {
    new DefaultColumnSet[K, T, V](datasetKey, timeColumn, mappings.map { case (a, b) => dataColumn(a) rename b })
  }

  override def |-> (columnName: String): ColumnSet[K, T, V] = {
    require(columnCountCompare(1) == 0, "|-> cannot operate on more than one column")
    new DefaultColumnSet[K, T, V](datasetKey, timeColumn, Seq(firstDataColumn rename columnName))
  }
}

trait ColumnSetWithAppend[K, T, V] { self: ColumnSet[K, T, V] =>
  def ++(that: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
    require(timeColumn eq that.timeColumn, "Both column sets must share the same time column")
    new DefaultColumnSet[K, T, V](datasetKey, timeColumn, allDataColumns ++ that.allDataColumns)
  }
}

trait DefaultColumnSetOps[K, T, V] extends ColumnSetWithSelect[K, T, V] with ColumnSetWithNew[K, T, V]
  with ColumnSetWithRename[K, T, V] with ColumnSetWithAppend[K, T, V] { self: ColumnSet[K, T, V] => }

object DefaultColumnSet {
  def apply[K, T, V](k: K, t: TimeSeriesTimeColumn[T], c: Seq[TimeSeriesDataColumn[V]]): DefaultColumnSet[K, T, V] = {
    new DefaultColumnSet[K, T, V](k, t, c)
  }

  def apply[K, T, V](k: K, t: Vector[T], c: Seq[(String, Vector[V])]): DefaultColumnSet[K, T, V] = {
    val timeColumn = new VectorTimeColumn[T](t)
    val len = timeColumn.length

    val cols = c.map { case (n, v) =>
      require(v.lengthCompare(len) == 0)
      new VectorDataColumn[V](n, v)
    }

    new DefaultColumnSet[K, T, V](k, timeColumn, cols)
  }
}

class DefaultColumnSet[K, T, V](val datasetKey: K, val timeColumn: TimeSeriesTimeColumn[T], cols: Seq[TimeSeriesDataColumn[V]])
  extends ColumnSet[K, T, V] with DefaultColumnSetOps[K, T, V] {

  override def columnNames: Seq[String] = cols.map(_.name)
  override def dataColumnOption(columnName: String): Option[TimeSeriesDataColumn[V]] = cols.find(_.name == columnName)
  override def dataColumnOption(columnIndex: Int): Option[TimeSeriesDataColumn[V]] = cols.lift(columnIndex)
  override def firstDataColumn: TimeSeriesDataColumn[V] = cols.head

  override def withIndexRange(from: Int, until: Int): ColumnSet[K, T, V] = {
    new DefaultColumnSet(datasetKey, timeColumn.slice(from, until), cols.map(_.slice(from, until)))
  }
}

class TimeSeriesDataset[K, T, V](m: TimeSeriesDatasetMetadata, val datasetKey: K, v: IndexedSeq[TimeSeriesData[K, T, V]])
  extends ColumnSet[K, T, V] with DefaultColumnSetOps[K, T, V] { self =>

  def columnNames: Seq[String] = m.columnNames

  val timeColumn: TimeSeriesTimeColumn[T] = new TCol(v)

  def dataColumnOption(columnName: String): Option[TimeSeriesDataColumn[V]] = {
    m.columnIndexOf(columnName).map(new DCol(v, columnName, _))
  }

  def dataColumnOption(columnIndex: Int): Option[TimeSeriesDataColumn[V]] = {
    m.columnNames.lift(columnIndex).map(new DCol(v, _, columnIndex))
  }

  override def firstDataColumn: TimeSeriesDataColumn[V] = new DCol(v, columnNames.head, 0)

  override def withIndexRange(from: Int, until: Int): ColumnSet[K, T, V] = {
    new TimeSeriesDataset(m, datasetKey, v.slice(from, until))
  }

  override def withIndexRange(from: Int): ColumnSet[K, T, V] = {
    new TimeSeriesDataset(m, datasetKey, v.slice(from, v.length))
  }

  protected[this] class TCol(v1: IndexedSeq[TimeSeriesData[K, T, V]]) extends TimeSeriesTimeColumn[T] {
    override def get(index: Int): T = v1(index).time
    override def length: Int = v1.length
    override def values: IndexedSeq[T] = v1.map(_.time)
    override def slice(from: Int, until: Int): TCol = new TCol(v1.slice(from, until))
  }

  protected[this] class DCol(v1: IndexedSeq[TimeSeriesData[K, T, V]], override val name: String, columnIndex: Int) extends TimeSeriesDataColumn[V] {
    override def length: Int = v1.length
    override def values: IndexedSeq[V] = v1.map(_.values(columnIndex))
    override def get(index: Int): V = v1(index).values(columnIndex)
    override def rename(to: String): TimeSeriesDataColumn[V] = new DCol(v1, to, columnIndex)
    override def slice(from: Int, until: Int): DCol = new DCol(v1.slice(from, until), name, columnIndex)
  }
}

trait TimeTypeRelated[T] extends Ordering[T]

object TimeTypeRelated {
  implicit object LocalDateRelated extends TimeTypeRelated[LocalDate] {
    override def compare(x: LocalDate, y: LocalDate): Int = x.compareTo(y)
  }

  implicit object LocalDateTimeRelated extends TimeTypeRelated[LocalDateTime] {
    override def compare(x: LocalDateTime, y: LocalDateTime): Int = x.compareTo(y)
  }
}

class TimeSeriesDatasetBuilder[K, T, V](metadata: TimeSeriesDatasetMetadata)(implicit ev: TimeTypeRelated[T])
  extends mutable.Builder[TimeSeriesData[K, T, V], Map[K, TimeSeriesDataset[K, T, V]]] { self =>

  val m = mutable.Map.empty[K, VectorBuilder[TimeSeriesData[K, T, V]]]
  var t: Option[T] = None

  override def += (row: TimeSeriesData[K, T, V]): this.type = {
    require(t.forall(ev.lteq(_, row.time)), "Must be sorted by time in ascending order")
    m.getOrElseUpdate(row.datasetKey, new VectorBuilder[TimeSeriesData[K, T, V]]) += row
    t = Some(row.time)
    self
  }

  override def result: Map[K, TimeSeriesDataset[K, T, V]] = m.map { case (k, vb) =>
    (k, new TimeSeriesDataset[K, T, V](metadata, k, vb.result))
  } .toMap

  override def clear(): Unit = {
    m.clear()
    t = None
  }
}
