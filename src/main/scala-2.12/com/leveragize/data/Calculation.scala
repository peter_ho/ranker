package com.leveragize.data

import scala.reflect.ClassTag

trait Calculation[K, T, V] { self =>
  def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V]
  def inputNames: Seq[String]
  def outputNames: Seq[String]
}

case class ChainedCalculations[K, T, V: ClassTag](calculations: Calculation[K, T, V]*)(implicit ev: DataDefaults[V])
  extends Calculation[K, T, V] {

  override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
    calculations.foldLeft(columnSet)(_ | _)
  }

  def inputNames: Seq[String] = calculations.head.inputNames
  def outputNames: Seq[String] = calculations.last.outputNames
}

abstract class DefaultCalculation[K, T, V](implicit classTagV: ClassTag[V], dataDefaults: DataDefaults[V]) extends Calculation[K, T, V] {
  def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
    val dataColumns = if (inputNames.lengthCompare(1) == 0 && columnSet.columnCountCompare(1) == 0) {
      // one expected input with one data column
      Seq(columnSet.firstDataColumn)
    }
    else {
      columnSet.dataColumns(inputNames)
    }

    val timeColumn = columnSet.timeColumn
    val len = timeColumn.length
    val nullValue = dataDefaults.nullValue
    val mappedOutputNames = outputNames
    val outputs = mappedOutputNames.map(_ => Array.fill(len)(nullValue))

    calculate(timeColumn, dataColumns, outputs)
    columnSet newColumnSet (outputNames zip outputs.map(_.to[Vector]))
  }

  def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit
}

trait SimpleCalculation[K, T, V] extends DefaultCalculation[K, T, V] {
  def calculate(timeColumn: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
    val output = outputs.head

    timeColumn.indices.foreach(i => output(i) = calculate(inputs, i))
  }

  def calculate(inputs: Seq[TimeSeriesDataColumn[V]], index: Int): V

  final def outputNames: Seq[String] = Seq(outputName)

  def outputName: String
}

abstract class FunctionSupport[F, V](val inputCount: Int) {
  def func(inputs: Seq[TimeSeriesDataColumn[V]], f: F): (Int) => V
}

object FunctionSupport {
  implicit object DoubleFunction1 extends FunctionSupport[(Double) => Double, Double](1) {
    override def func(inputs: Seq[TimeSeriesDataColumn[Double]], f: (Double) => Double): (Int) => Double = {
      val Seq(x) = inputs

      i => f(x(i))
    }
  }

  implicit object DoubleFunction2 extends FunctionSupport[(Double, Double) => Double, Double](2) {
    override def func(inputs: Seq[TimeSeriesDataColumn[Double]], f: (Double, Double) => Double): (Int) => Double = {
      val Seq(x1, x2) = inputs

      i => f(x1(i), x2(i))
    }
  }

  implicit object DoubleFunction3 extends FunctionSupport[(Double, Double, Double) => Double, Double](3) {
    override def func(inputs: Seq[TimeSeriesDataColumn[Double]], f: (Double, Double, Double) => Double): (Int) => Double = {
      val Seq(x1, x2, x3) = inputs

      i => f(x1(i), x2(i), x3(i))
    }
  }

  implicit object DoubleFunction4 extends FunctionSupport[(Double, Double, Double, Double) => Double, Double](4) {
    override def func(inputs: Seq[TimeSeriesDataColumn[Double]], f: (Double, Double, Double, Double) => Double): (Int) => Double = {
      val Seq(x1, x2, x3, x4) = inputs

      i => f(x1(i), x2(i), x3(i), x4(i))
    }
  }

  implicit object DoubleFunction5 extends FunctionSupport[(Double, Double, Double, Double, Double) => Double, Double](5) {
    override def func(inputs: Seq[TimeSeriesDataColumn[Double]], f: (Double, Double, Double, Double, Double) => Double): (Int) => Double = {
      val Seq(x1, x2, x3, x4, x5) = inputs

      i => f(x1(i), x2(i), x3(i), x4(i), x5(i))
    }
  }
}

class Function[K, T, V: ClassTag, F](val inputNames: Seq[String], arity: Int)(f: F)
                                 (implicit fs: FunctionSupport[F, V], ev: DataDefaults[V]) extends DefaultCalculation[K, T, V] {

  require(inputNames.lengthCompare(fs.inputCount) == 0)

  override def outputNames: Seq[String] = Seq("output")

  override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
    val y = outputs.head
    val g = fs.func(inputs, f)

    time.indices.foreach(i => y(i) = g(i))
  }
}

case class FunctionN[K, T, V: ClassTag](override val inputNames: Seq[String])(f: Seq[V] => V)(implicit ev: DataDefaults[V])
  extends DefaultCalculation[K, T, V] {

  override def outputNames: Seq[String] = Seq("output")

  override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
    val y = outputs.head

    time.indices.foreach(i => y(i) = f(inputs.map(x => x(i))))
  }
}