package com.leveragize.data

import com.leveragize.stat._

/**
  * Created by peterho on 3/20/17.
  */

object Quantiles {
  def apply(ds: DailyDataset, calculation: DailyCalculation): Quantiles = new Quantiles(ds, calculation)
}

class Quantiles(ds: DailyDataset, calculation: DailyCalculation) {
  private[this] lazy val sortedPairs = {
    (ds | calculation).firstDataColumn.values.zipWithIndex.filterNot(t => t._1.isNaN).sortBy(_._1)
  }

  def get(pList: Seq[Double]): Seq[Double] = pList map { quantile(sortedPairs.unzip._1, _, sorted = true) }

  def get[G](pList: Seq[Double], calc: DailyCalculation)(classifier: PartialFunction[Double, G]): Map[G, (Seq[Double], Int)] = {
    val v = (ds | calc).firstDataColumn.values

    sortedPairs groupBy { case (_, i) =>
      val x = v(i)

      if (classifier.isDefinedAt(x)) Some(classifier(x)) else None
    } collect { case (Some(g), pairs) =>
      val values = pairs.unzip._1

      g -> (pList.map { quantile(values, _, sorted = true) }, values.length)
    }
  }

  def printSummary[G](pList: Seq[Double]): Unit = {
    println(get(pList).map(q => f"$q%5.3f").mkString(", "))
  }

  def printSummary[G](pList: Seq[Double], calc: DailyCalculation)(classifier: PartialFunction[Double, G]): Unit = {
    val map = get(pList, calc)(classifier)

    map.foreach { case (g, (qList, n)) =>
      val str = qList.map(q => f"$q%5.3f").mkString(", ")

      println(s"$g ($n) => $str")
    }
  }
}

