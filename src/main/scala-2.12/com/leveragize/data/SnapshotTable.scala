package com.leveragize.data

import java.time.LocalDate
import java.util.UUID

import com.leveragize.common.StopWatch
import com.leveragize.data.sql._

import scalikejdbc._
import jsr310._

import spray.json.{DefaultJsonProtocol, JsonFormat}

/**
  * Created by peterho on 7/31/17.
  */
case class SnapshotTable(id: String, data: Map[String, Option[Double]])


object SnapshotTable {
  def get(date: LocalDate): Seq[SnapshotTable] = DB readOnly { implicit session =>
    val (fieldNames, sqlNames) = dataFields.unzip
    val list = SQLSyntax.join(sqlNames, sqls",", spaceBeforeDelimier = false)

    StopWatch.printElapsedTime("SnapshotTable.list") {
      sql"SELECT sym_id, $list FROM snapshot_table WHERE date = $date"
        .map { rs =>
          val id = rs.get[UUID](1)
          val data = fieldNames.zipWithIndex map { case (s, i) => s -> rs.doubleOpt(i + 2) }

          SnapshotTable(id.toString, data.toMap)
        }
        .list()
        .apply()
    }
  }

  val dataFields: Seq[(String, SQLSyntax)] =
    Seq("close" -> sqls"close", "volume" -> sqls"volume", "iv30" -> sqls"iv30",
      "iv30_iv90" -> sqls"iv30_iv90", "iv30_hv" -> sqls"iv30_hv") ++ DailyIndicator.allDataFields
}

object SnapshotTableProtocol extends DefaultJsonProtocol {
  implicit val format: JsonFormat[SnapshotTable] = jsonFormat2(SnapshotTable.apply)
}
