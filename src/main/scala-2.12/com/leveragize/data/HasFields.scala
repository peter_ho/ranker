package com.leveragize.data

import scalikejdbc.interpolation.SQLSyntax

/**
  * Created by peterho on 4/24/17.
  */
trait HasFields {
  def allDataFields: Seq[(String, SQLSyntax)]
  def dataFields(list: String*): Seq[(String, SQLSyntax)] = allDataFields.filter(t => list.contains(t._1))
  def dataFieldsExcept(list: String*): Seq[(String, SQLSyntax)] = allDataFields.filterNot(t => list.contains(t._1))
}
