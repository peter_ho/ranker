package com.leveragize.data

import scalikejdbc._

/**
  * Created by peterho on 4/19/17.
  */

object DailyData extends HasFields {
  val allDataFields = Seq("open" -> sqls"open", "high" -> sqls"high", "low" -> sqls"low", "close" -> sqls"close",
    "volume" -> sqls"volume", "iv10c" -> sqls"iv10c", "iv10p" -> sqls"iv10p", "iv30c" -> sqls"iv30c", "iv30p" -> sqls"iv30p",
    "iv90c" -> sqls"iv90c", "iv90p" -> sqls"iv90p", "skew30" -> sqls"skew30")
}
