package com.leveragize.data

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.UUID

import com.leveragize.common.StopWatch
import scalikejdbc._
import jsr310._
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, RootJsonFormat}

/**
  * Created by peterho on 4/24/17.
  */
case class DailyAnalyticsRow(date: LocalDate, data: Map[String, Option[Double]])
case class DailyAnalytics(id: String, rows: Seq[DailyAnalyticsRow])


object DailyAnalytics {
  def list(symId: String): Seq[DailyAnalyticsRow] = DB readOnly { implicit session =>
    val (ddFieldNames, ddSqlNames) = DailyData.allDataFields.unzip
    val (diFieldNames, diSqlNames) = DailyIndicator.allDataFields.unzip

    val list = SQLSyntax.join(ddSqlNames.map(x => sqls"dd.$x") ++ diSqlNames.map(x => sqls"di.$x"), sqls",", spaceBeforeDelimier = false)

    StopWatch.printElapsedTime("DailyAnalytics.list") {
      sql"""
           SELECT dd.date, $list FROM daily_indicator di
           INNER JOIN daily_data dd ON (dd.sym_id, dd.date) = (di.sym_id, di.date) AND dd.sym_id = ${UUID.fromString(symId)}
           ORDER BY dd.date
        """
        .map { rs =>
          val date = rs.localDate(1)
          val data = (ddFieldNames ++ diFieldNames).zipWithIndex map { case (s, i) => s -> rs.doubleOpt(i + 2) }

          DailyAnalyticsRow(date, data.toMap)
        }
        .list()
        .apply()
    }
  }

  def get(symId: String): DailyAnalytics = DailyAnalytics(symId, list(symId))
}

object DailyAnalyticsProtocol extends DefaultJsonProtocol {
  implicit val localDateFormat = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue = JsString(formatter.format(obj))

    override def read(json: JsValue): LocalDate = {
      json match {
        case JsString(s) => LocalDate.parse(s, formatter)
        case _ => throw new Error
      }
    }

    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE
  }

  implicit val jsonDailyAnalyticsRow: JsonFormat[DailyAnalyticsRow] = jsonFormat2(DailyAnalyticsRow.apply)
  implicit val jsonDailyAnalytics: RootJsonFormat[DailyAnalytics] = jsonFormat2(DailyAnalytics.apply)
}
