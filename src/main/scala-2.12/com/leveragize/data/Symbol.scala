package com.leveragize.data

import java.util.UUID

import com.leveragize.common.StopWatch
import com.leveragize.data.sql._
import scalikejdbc._
import spray.json.{DefaultJsonProtocol, JsonFormat}

/**
  * Created by peterho on 5/10/17.
  */
case class Symbol(id: String, ticker: String, name: String)

object Symbol {
  def list: Seq[Symbol] = DB readOnly { implicit session =>
    StopWatch.printElapsedTime("Symbol.list") {
      sql"""
           SELECT st.id, st.symbol, st.name FROM sym_table st ORDER BY st.name
        """
        .map { rs =>
          val id = rs.get[UUID](1)
          val ticker = rs.string(2)
          val name = rs.string(3)

          Symbol(id.toString, ticker, name)
        }
        .list()
        .apply()
    }
  }
}

object SymbolProtocol extends DefaultJsonProtocol {
  implicit val format: JsonFormat[Symbol] = jsonFormat3(Symbol.apply)
}
