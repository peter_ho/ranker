package com.leveragize.data

/**
  * Created by peterho on 1/22/17.
  */
case class RangeCondition[T](from: RangePoint[T] = Open, to: RangePoint[T] = Open) {
  def translate[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Seq[C] = {
    Seq(from.startCondition(ref), to.endCondition(ref)).flatten
  }
}

trait RangePoint[+T] {
  def startCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Option[C]
  def endCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Option[C]
}

case class InclusiveOf[T](value: T) extends RangePoint[T] {
  def startCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Option[C] = Some(ev.gte(ref, value))
  def endCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Option[C] = Some(ev.lte(ref, value))
}

case class ExclusiveOf[T](value: T) extends RangePoint[T] {
  def startCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Option[C] = Some(ev.gt(ref, value))
  def endCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, T, C]): Option[C] = Some(ev.lt(ref, value))
}

case object Open extends RangePoint[Nothing] {
  def startCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, Nothing, C]): Option[C] = None
  def endCondition[S, C](ref: S)(implicit ev: RangeConditionRelated[S, Nothing, C]): Option[C] = None
}

trait RangeConditionRelated[-S, -T, C] {
  def lt(left: S, right: T): C
  def gt(left: S, right: T): C
  def lte(left: S, right: T): C
  def gte(left: S, right: T): C
}
