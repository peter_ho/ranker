package com.leveragize.data

import java.util.UUID

import com.leveragize.common.StopWatch
import com.leveragize.data.sql._
import scalikejdbc._
import spray.json.{DefaultJsonProtocol, JsonFormat}

/**
  * Created by peterho on 7/31/17.
  */
case class SummaryTable(id: String, symbol: String, name: String, data: Map[String, Option[Double]])


object SummaryTable {
  def list: Seq[SummaryTable] = DB readOnly { implicit session =>
    val (fieldNames, sqlNames) = dataFields.unzip
    val list = SQLSyntax.join(sqlNames, sqls",", spaceBeforeDelimier = false)

    StopWatch.printElapsedTime("SummaryTable.list") {
      sql"SELECT sym_id, symbol, name, $list FROM summary_table WHERE length > 3 AND null_pct < 0.05"
        .map { rs =>
          val id = rs.get[UUID](1)
          val symbol = rs.string(2)
          val name = rs.string(3)
          val data = fieldNames.zipWithIndex map { case (s, i) => s -> rs.doubleOpt(i + 4) }

          SummaryTable(id.toString, symbol, name, data.toMap)
        }
        .list()
        .apply()
    }
  }

  val dataFields: Seq[(String, SQLSyntax)] = Seq(
    "ret_co_oc", "ret_ma20", "ret_ma50", "ret_ma200",
    "call_mov21_hi", "call_mov21_lo", "put_mov21_hi", "put_mov21_lo",
    "call_er21_hi", "call_er21_lo", "put_er21_hi", "put_er21_lo",
    "call_rsi10_hi", "call_rsi10_lo", "put_rsi10_hi", "put_rsi10_lo",
    "call_iv30_iv90_hi", "call_iv30_iv90_lo", "put_iv30_iv90_hi", "put_iv30_iv90_lo",
    "call_iv30_hv_hi", "call_iv30_hv_lo", "put_iv30_hv_hi", "put_iv30_hv_lo",
    "call", "put", "ret_avg", "length", "null_pct"
  ).map(f => f -> StringContext(f).sqls())

  val idFields: Seq[(String, SQLSyntax)] = Seq("sym_id", "symbol", "name").map(f => f -> StringContext(f).sqls())

  val allFields: Seq[(String, SQLSyntax)] = idFields ++ dataFields
}

object SummaryTableProtocol extends DefaultJsonProtocol {
  implicit val format: JsonFormat[SummaryTable] = jsonFormat4(SummaryTable.apply)
}
