package com.leveragize.data

import java.time.LocalDate

import com.leveragize.common.StopWatch
import scalikejdbc._
import spray.json.{DefaultJsonProtocol, JsonFormat}

/**
  * Created by peterho on 4/19/17.
  */
case class DailyIndicatorSummary(ticker: String, name: String, data: Map[String, Option[Double]])

object DailyIndicatorSummary {
  def list(date: LocalDate): Seq[DailyIndicatorSummary] = DB readOnly { implicit session =>
    val (ddFieldNames, ddSqlNames) = DailyData.dataFields("close", "iv10", "iv30", "iv90").unzip
    val (diFieldNames, diSqlNames) = DailyIndicator.allDataFields.unzip

    val list = SQLSyntax.join(ddSqlNames.map(x => sqls"dd.$x") ++ diSqlNames.map(x => sqls"di.$x"), sqls",", spaceBeforeDelimier = false)

    StopWatch.printElapsedTime("DailyIndicatorSummary.list") {
      sql"""
           SELECT st.symbol, st.name, $list FROM daily_indicator di
           INNER JOIN sym_table st ON st.id = di.sym_id AND di.date = $date
           INNER JOIN daily_data dd ON (dd.sym_id, dd.date) = (di.sym_id, di.date)
        """
        .map { rs =>
          val ticker = rs.string(1)
          val name = rs.string(2)
          val data = (ddFieldNames ++ diFieldNames).zipWithIndex map { case (s, i) => s -> rs.doubleOpt(i + 3) }

          DailyIndicatorSummary(ticker, name, data.toMap)
        }
        .list()
        .apply()
    }
  }
}

object DailyIndicatorSummaryProtocol extends DefaultJsonProtocol {
  implicit val format: JsonFormat[DailyIndicatorSummary] = jsonFormat3(DailyIndicatorSummary.apply)
}
