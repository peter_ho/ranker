package com.leveragize.data

/**
  * Created by peterho on 1/23/17.
  */
trait DataDefaults[V] {
  def invalidValue: V
  def nullValue: V
  def replaceInvalid(value: V, replacement: V): V
  def makeOption(value: V): Option[V] = Some(value)
  def isNullOrInvalid(value: V): Boolean
}

object DataDefaults {
  implicit object DoubleDefaults extends DataDefaults[Double] {
    def invalidValue: Double = Double.NaN
    def nullValue: Double = Double.NaN
    def replaceInvalid(value: Double, replacement: Double): Double = if (value.isNaN) replacement else value

    override def makeOption(value: Double): Option[Double] = {
      if (value.isNaN || value.isInfinity) None else Some(value)
    }

    def isNullOrInvalid(value: Double): Boolean = value.isNaN
  }

  implicit object StringDefaults extends DataDefaults[String] {
    def invalidValue: String = ""
    def nullValue: String = ""
    def replaceInvalid(value: String, replacement: String): String = if (value.isEmpty) replacement else value
    def isNullOrInvalid(value: String): Boolean = value.lengthCompare(0) == 0
  }

  implicit object AnyDefaults extends DataDefaults[Any] {
    def invalidValue: Any = throw new RuntimeException
    def nullValue: Any = throw new RuntimeException
    def replaceInvalid(value: Any, replacement: Any): Any = throw new RuntimeException
    def isNullOrInvalid(value: Any): Boolean = throw new RuntimeException
  }
}
