package com.leveragize.data

import java.time.LocalDate
import java.util.UUID

import com.leveragize.stat

import scala.math._
import com.leveragize.stat._
import org.apache.commons.math3.distribution.TDistribution

/**
  * Created by peterho on 1/25/17.
  */
object DailyIndicators {
  type K = UUID
  type T = LocalDate
  type V = Double

  def countLeadNaN(column: TimeSeriesDataColumn[V]): Int = {
    val n = column.indexWhere(!_.isNaN)

    if (n < 0) column.length else n
  }

  def function(f: (V) => V)(implicit fs: FunctionSupport[(V) => V, V]): Calculation[K, T, V] = {
    function("x")(f)(fs)
  }

  def function(n1: String)(f: (V) => V)(implicit fs: FunctionSupport[(V) => V, V]): Calculation[K, T, V] = {
    new Function[K, T, V, (V) => V](Seq(n1), 1)(f)
  }

  def function(n1: String, n2: String)(f: (V, V) => V)(implicit fs: FunctionSupport[(V, V) => V, V]): Calculation[K, T, V] = {
    new Function[K, T, V, (V, V) => V](Seq(n1, n2), 2)(f)
  }

  def function(n1: String, n2: String, n3: String)(f: (V, V, V) => V)(implicit fs: FunctionSupport[(V, V, V) => V, V]): Calculation[K, T, V] = {
    new Function[K, T, V, (V, V, V) => V](Seq(n1, n2, n3), 3)(f)
  }

  def function(n1: String, n2: String, n3: String, n4: String)(f: (V, V, V, V) => V)(implicit fs: FunctionSupport[(V, V, V, V) => V, V]): Calculation[K, T, V] = {
    new Function[K, T, V, (V, V, V, V) => V](Seq(n1, n2, n3, n4), 4)(f)
  }

  def function(n1: String, n2: String, n3: String, n4: String, n5: String)(f: (V, V, V, V, V) => V)(implicit fs: FunctionSupport[(V, V, V, V, V) => V, V]): Calculation[K, T, V] = {
    new Function[K, T, V, (V, V, V, V, V) => V](Seq(n1, n2, n3, n4, n5), 5)(f)
  }

  case class SMA(n: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputNames = Seq("sma")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val x = inputs.head
      val sma = outputs.head
      val nLeadNaN = countLeadNaN(x)

      var sum = 0.0

      t.indices.foreach { i =>
        sum = sum + x(i, 0.0) - x(i - n, 0.0)
        sma(i) = if (i < n - 1 + nLeadNaN) Double.NaN else sum / n
      }
    }
  }

  case class Lead(n: Int) extends SimpleCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputName = "lead"

    override def calculate(inputs: Seq[TimeSeriesDataColumn[V]], index: Int): V = {
      inputs match { case Seq(x) => x(index + n) }
    }
  }

  case class Lag(n: Int) extends SimpleCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputName = "lag"

    override def calculate(inputs: Seq[TimeSeriesDataColumn[V]], index: Int): V = {
      inputs match { case Seq(x) => x(index - n) }
    }
  }

  case class RSI(n: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputNames = Seq("rsi")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val x = inputs.head
      val rsi = outputs.head
      val nLeadNaN = countLeadNaN(x)

      def retPos(i: Int) = 0.0 max (x(i, 0.0) - x(i - 1, 0.0))
      def retNeg(i: Int) = 0.0 min (x(i, 0.0) - x(i - 1, 0.0))

      var sumUp = 0.0
      var sumDn = 0.0

      (nLeadNaN until t.length).foreach { i =>
        sumUp += retPos(i) - retPos(i - n)
        sumDn += retNeg(i - n) - retNeg(i)
        rsi(i) = if (i < n + nLeadNaN) Double.NaN else 100.0 - 100.0 / (1.0 + sumUp / sumDn)
      }
    }
  }

  case class PercentRank(n: Int, multiplier: Double = 0.5) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("x")
    override val outputNames = Seq("pr")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val x = inputs.head
      val pr = outputs.head


      (n - 1 + countLeadNaN(x) until t.length).foreach { i =>
        var cL = 0
        var f = 0
        val xi = x(i)

        (i - n + 1 to i).foreach { j =>
          val xj = x(j)

          if (xj < xi) cL += 1
          if (xj == xi) f += 1
        }

        pr(i) = (cL + 0.5 * f) / n
      }
    }
  }

  case class VolatilityRS(n: Int, z: Int = 252) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("open", "high", "low", "close")
    override val outputNames = Seq("rs")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val nLeadNaN = inputs.map(countLeadNaN).max
      val Seq(open, high, low, close) = inputs
      val vol = outputs.head
      var sum = 0.0

      def x(i: Int): V = {
        val y = log(high(i) / close(i)) * log(high(i) / open(i)) + log(low(i) / close(i)) * log(low(i) / open(i))

        if (y.isNaN || y.isInfinity) 0.0 else y
      }

      t.indices.foreach { i =>
        sum += x(i) - x(i - n)
        vol(i) = if (i < n - 1 + nLeadNaN) Double.NaN else sqrt(z * sum / n)
      }
    }
  }

  case class ROC(n: Int) extends SimpleCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputName = "roc"

    override def calculate(inputs: Seq[TimeSeriesDataColumn[V]], index: Int): V = {
      val x = inputs.head

      if (n > 0) {
        log(x(index) / x(index - n))
      }
      else {
        log(x(index - n) / x(index))
      }
    }
  }

  case class LogDiff(xName: String, yName: String, xLag: Int = 0) extends SimpleCalculation[K, T, V] {
    override val inputNames = Seq(xName, yName)
    override val outputName = "diff"

    override def calculate(inputs: Seq[TimeSeriesDataColumn[V]], index: Int): V = {
      val Seq(x, y) = inputs

      log(y(index)) - log(x(index - xLag))
    }
  }

  case class MeanVariance(n: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputNames = Seq("mean", "var")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val x = inputs.head
      val Seq(meanCol, varianceCol) = outputs
      val nLeadNaN = countLeadNaN(x)

      var mean = 0.0
      var variance = 0.0

      // Welford's Method
      t.indices.foreach { i =>
        val oldValue = x(i - n, 0.0)
        val newValue = x(i, 0.0)
        val oldMean = mean
        val newMean = oldMean + (newValue - oldValue) / n

        mean = newMean
        variance += (newValue - oldValue) * (newValue - newMean + oldValue - oldMean) / (n - 1)
        meanCol(i) = if (i < n - 1 + nLeadNaN) Double.NaN else mean
        varianceCol(i) = if (i < n - 1 + nLeadNaN) Double.NaN else variance
      }
    }
  }

  case class VolatilityCC(q: Int, k: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputNames = Seq(s"cc$q")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val close = inputs.head
      val cc = outputs.head

      def sq(value: Double) = value * value
      def x(index: Int) = log(close(index))

      val n = k * q
      val factor = k.toDouble / ((n.toDouble - q.toDouble + 1.0) * (k.toDouble - 1.0))

      t.indices.foreach { i =>
        val mu = (x(i) - x(i - n)) / n.toDouble

        cc(i) = sqrt(factor * (i - n + q to i).foldLeft(0.0)((sum, j) => sum + sq(x(j) - x(j - q) - q * mu)))
      }
    }
  }

  case class VarianceRatio(n: Int = 256, qList: Seq[Int] = Seq(1, 2, 4, 8, 16, 32, 64)) extends DefaultCalculation[K, T, V] {
    require(qList.forall(n % _ == 0), "[n] must be divisible by [q]")

    override def inputNames: Seq[String] = Seq("close")
    override def outputNames: Seq[String] = Seq("hurst", "err")

    override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val close = inputs.head
      val Seq(hurst, err) = outputs
      val x = close.values.map(log)

      def sq(value: Double) = value * value

      def calcVar(i: Int, q: Int) = {
        val k = n / q
        val factor = k / ((n - q + 1.0) * (k - 1.0))
        val mu = (x(i) - x(i - n)) / k

        factor * (i - n + q to i).foldLeft(0.0)((sum, j) => sum + sq(x(j) - x(j - q) - mu))
      }

      val logPeriods = qList.map(q => log(q))

      (n until time.length).foreach { i =>
        val logVars = qList.map(q => log(calcVar(i, q)))
        val lm = LinearModel(logPeriods zip logVars)

        hurst(i) = lm.beta / 2.0
        err(i) = lm.errBeta / 2.0
      }
    }
  }

  case class SimpleVarianceRatio(q: Int, k: Int) extends DailyCalculation {
    override def inputNames: Seq[String] = Seq("close")
    override def outputNames: Seq[String] = Seq("vr")

    override def apply(ds: DailyDataset): ColumnSet[K, T, V] = {
      val n = k * q
      val vol1 = ds | VolatilityCC(1, n) | function("_")(_ * sqrt(q)) |-> "vol1"
      val volQ = ds | VolatilityCC(q, k) |-> "volQ"

      vol1 ++ volQ |++ RunLM("vol1", "volQ", 3 * n) | function("alpha", "beta", "errResidual", "vol1", "volQ") { (b, m, err, x, y) =>
        (m * x + b - y) / err
      } |-> "vr"
    }
  }

  case class RunHurst(q: Int, n: Int) extends DefaultCalculation[K, T, V] {
    override def inputNames: Seq[String] = Seq("close")
    override def outputNames: Seq[String] = Seq("hurst", "err")

    override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val close = inputs.head
      val Seq(hurst, err) = outputs
      val x = close.values.map(log)

      def calcVar(i: Int, q: Int) = {
        val k = n / q
        val factor = k / ((n - q + 1.0) * (k - 1.0))
        val mu = (x(i) - x(i - n)) / k

        factor * (i - n + q to i).foldLeft(0.0)((sum, j) => sum + sq(x(j) - x(j - q) - mu))
      }

      def logPair(q: Int, v: Double) = (log(q), log(v))

      (n until time.length).foreach { i =>
        val lm = LinearModel(Seq(logPair(1, calcVar(i, 1)), logPair(q, calcVar(i, q))))

        hurst(i) = lm.beta / 2.0
        err(i) = lm.errBeta / 2.0
      }
    }
  }

  case class EfficiencyRatio(n: Int, signed: Boolean = false) extends DefaultCalculation[K, T, V] {
    override val inputNames = Seq("close")
    override val outputNames = Seq("er")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val close = inputs.head
      val er = outputs.head

      var sum = 0.0

      def x(i: Int) = abs(close(i, 0.0) - close(i - 1, 0.0))

      t.indices.foreach { i =>
        val delta = close(i) - close(i - n)
        val diff = if (signed) delta else abs(delta)

        sum += x(i) - x(i - n)
        er(i) = diff / sum
      }
    }
  }

  case class VolatilityYZ(n: Int, z: Int = 252) extends Calculation[K, T, V] {
    override val inputNames = Seq("open", "high", "low", "close")
    override val outputNames = Seq("yz")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val varCO = columnSet | LogDiff("close", "open", 1) | MeanVariance(n) |* "var" -> "varCO"
      val varOC = columnSet | LogDiff("open", "close") | MeanVariance(n) |* "var" -> "varOC"
      val rs = columnSet.select("open", "high", "low", "close") | VolatilityRS(n, z) |-> "rs"
      val k = 0.34 / (1.34 + (n + 1.0) / (n - 1.0))

      (varCO ++ varOC ++ rs) | function("varCO", "varOC", "rs") { (vco, voc, r) =>
        sqrt(z * vco + z * k * voc + (1.0 - k) * r * r)
      } |-> outputNames.head
    }
  }

  case class ForwardFill(columns: String*) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = columns
    override val outputNames: Seq[String] = columns

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val vectors = columnSet.dataColumns(inputNames).map { col =>
        col.values.scanLeft(Double.NaN)((a, b) => if (b.isNaN) a else b).drop(1).toVector
      }

      columnSet newColumnSet (outputNames zip vectors)
    }
  }

  case class RunAgg(n: Int, func: IndexedSeq[V] => V) extends DefaultCalculation[K, T, V] {
    require(n > 0)

    override val inputNames: Seq[String] = Seq("x")
    override val outputNames: Seq[String] = Seq("agg")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val x = inputs.head
      val agg = outputs.head
      val len = agg.length

      if (len < n) {
        (0 until len).foreach(i => agg(i) = Double.NaN)
      }
      else {
        (0 until n - 1).foreach(i => agg(i) = Double.NaN)
        x.values.sliding(n).zipWithIndex.foreach { case (window, i) => agg(i + n - 1) = func(window) }
      }
    }
  }

  case class SigmaMoves(n: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("high", "low", "close", "sigma")
    override val outputNames: Seq[String] = Seq(s"ret", s"upe", s"dne")

    override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val scale = sqrt(abs(n))
      val sigma = inputs.last.values map (_ * scale)
      val Seq(logHigh, logLow, logClose) = inputs take 3 map (_.values map log)
      val Seq(ret, upe, dne) = outputs
      val len = ret.length

      def f(startIndex: Int, endIndex: Int) = {
        val c0 = logClose(startIndex)
        var hh = c0
        var ll = c0

        for (i <- startIndex + 1 to endIndex) {
          hh = hh max logHigh(i)
          ll = ll min logLow(i)
        }

        (logClose(endIndex) - c0, hh - c0, ll - c0)
      }

      val (indexRange, startOffset, endOffset) = if (n > 0) (n until len, -n, 0) else (0 until len + n, 0, -n)

      for (i <- indexRange) {
        val startIndex = i + startOffset
        val endIndex = i + endOffset
        val (r, u, d) = f(startIndex, endIndex)

        ret(i) = r / sigma(startIndex)
        upe(i) = u / sigma(startIndex)
        dne(i) = d / sigma(startIndex)
      }
    }
  }

  case class RunCov(input1: String, input2: String, n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(input1, input2)
    override val outputNames: Seq[String] = Seq("cov", "mean1", "mean2")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val mean1 = (columnSet.select(input1) | SMA(n)).firstDataColumn.values
      val mean2 = (columnSet.select(input2) | SMA(n)).firstDataColumn.values
      val r1 = columnSet.dataColumn(input1).values
      val r2 = columnSet.dataColumn(input2).values
      val len = columnSet.timeColumn.length
      val cov = Array.ofDim[V](len)

      for (i <- 0 until len) {
        val m1 = mean1(i)
        val m2 = mean2(i)

        cov(i) = if (m1.isNaN || m2.isNaN) {
          Double.NaN
        }
        else {
          (i - n + 1 to i).foldLeft(0.0) { (acc, j) => acc + (r1(j) - m1) * (r2(j) - m2) } / (n - 1.0)
        }
      }

      columnSet newColumnSet (outputNames zip Seq(cov.to[Vector], mean1.to[Vector], mean2.to[Vector]))
    }
  }

  case class RunSum(n: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("x")
    override val outputNames: Seq[String] = Seq("sum")

    override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val x = inputs.head
      val sumX = outputs.head
      val nLeadNaN = countLeadNaN(x)

      var sum = 0.0

      for (i <- x.indices) {
        sum += x(i, 0.0) - x(i - n, 0.0)
        sumX(i) = if (i < n - 1 + nLeadNaN) Double.NaN else sum
      }
    }
  }

  case class RunLM(input1: String, input2: String, n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(input1, input2)
    override val outputNames: Seq[String] = Seq("alpha", "beta", "r2", "errAlpha", "errBeta", "errResidual")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val x = columnSet.select(input1)
      val y = columnSet.select(input2)
      val covXY = columnSet | RunCov(input1, input2, n) select "cov"
      val meanVarX = x | MeanVariance(n)
      val meanVarY = y | MeanVariance(n)

      val beta = (covXY ++ meanVarX) | function("cov", "var") { (c, v) => c / v } |-> "beta"

      val alpha = (beta ++ (meanVarX |* "mean" -> "meanX") ++ (meanVarY |* "mean" -> "meanY")) |
        function("beta", "meanX", "meanY") { (b, mX, mY) => mY - b * mX } |-> "alpha"

      val x2 = columnSet | function(input1)(sq) |-> "x2"
      val y2 = columnSet | function(input2)(sq) |-> "y2"

      val r2 =
        (beta ++ meanVarY.select("var") ++ (x | RunSum(n) |-> "sumX") ++ (y | RunSum(n) |-> "sumY") ++
        (x2 | RunSum(n) |-> "sumX2") ++ (y2 | RunSum(n) |-> "sumY2")) |
        FunctionN(Seq("beta", "var", "sumX", "sumY", "sumX2", "sumY2")) { case Seq(b, varY, sumX, sumY, sumX2, sumY2) =>
          val res = 1.0 / (n * (n - 2.0)) * (n * sumY2 - sq(sumY) - sq(b) * (n * sumX2 - sq(sumX)))

          1.0 - res / (varY * (n - 1.0) / (n - 2.0))
        } |-> "r2"

      val errResidual = (r2 ++ meanVarY) | function("r2", "var") { (r2, varY) => sqrt(varY * (1.0 - r2) * (n - 1.0) / (n - 2.0)) } |-> "errResidual"

      val errAlpha = errResidual ++ meanVarX | function("errResidual", "mean", "var") { (s, meanX, varX) =>
        s / sqrt(n) * sqrt(1.0 + sq(meanX) / (varX * (n - 1.0) / n))
      } |-> "errAlpha"

      val errBeta = errResidual ++ meanVarX | function("errResidual", "var") { (s, varX) =>
        s / sqrt(varX * (n - 1.0))
      } |-> "errBeta"

      alpha ++ beta ++ r2 ++ errAlpha ++ errBeta ++ errResidual
    }
  }

  object TimeIndex extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Nil
    override val outputNames: Seq[String] = Seq("idx")

    override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val idx = outputs.head

      for (i <- time.indices) {
        idx(i) = i
      }
    }
  }

  case class TimeTrend(n: Int, continuous: Boolean = true) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("close")
    override val outputNames: Seq[String] = Seq("t", "slope", "errSlope")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val x = if (continuous) columnSet | function("close")(log) |-> "x" else columnSet |* "close" -> "x"
      val idx = columnSet | TimeIndex |-> "idx"
      val sfm = (x ++ idx) | RunLM("idx", "x", n)
      val t = sfm | function("beta", "errBeta") { (beta, err) => beta / err } |-> "t"

      (t ++ sfm) |* ("t" -> "t", "beta" -> "slope", "errBeta" -> "errSlope")
    }
  }

  case class LinearRegressionChannel(n: Int, mult: Double = 2) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("close")
    override val outputNames: Seq[String] = Seq("upper", "middle", "lower", "slopeT")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val x = columnSet |* "close" -> "x"
      val idx = columnSet | TimeIndex |-> "idx"
      val sfm = (x ++ idx) | RunLM("idx", "x", n)
      val middle = (sfm ++ idx) | function("alpha", "beta", "idx") { (a, b, i) => a + b * i } |-> "middle"
      val upper = (sfm ++ middle) | function("middle", "errResidual") { (est, err) => est + mult * err } |-> "upper"
      val lower = (sfm ++ middle) | function("middle", "errResidual") { (est, err) => est - mult * err } |-> "lower"
      val slopeT = sfm | function("beta", "errBeta")(_ / _) |-> "slopeT"

      upper ++ middle ++ lower ++ slopeT
    }
  }

  object TrueRange extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("high", "low", "close")
    override val outputNames: Seq[String] = Seq("tr")

    override def calculate(time: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val Seq(high, low, close) = inputs
      val tr = outputs.head

      for (i <- 1 until time.length) {
        tr(i) = (high(i) - low(i)) max (high(i) - close(i - 1)) max (close(i - 1) - low(i))
      }
    }
  }

  case class VortexIndicator(n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("high", "low", "close")
    override val outputNames: Seq[String] = Seq("pVM", "nVM")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val high = columnSet select "high"
      val high1 = high | Lag(1) |-> "high1"
      val low = columnSet select "low"
      val low1 = low | Lag(1) |-> "low1"
      val pSum = (high ++ low1) | function("high", "low1") { (a, b) => abs(a - b) } | RunSum(n) |-> "pSum"
      val nSum = (low ++ high1) | function("low", "high1") { (a, b) => abs(a - b) } | RunSum(n) |-> "nSum"
      val trSum = columnSet | TrueRange | RunSum(n) |-> "trSum"
      val pVM = pSum ++ trSum | function("pSum", "trSum")(_ / _) |-> "pVM"
      val nVM = nSum ++ trSum | function("nSum", "trSum")(_ / _) |-> "nVM"

      (pVM ++ nVM).select("pVM", "nVM")
    }
  }

  object ChaosIndicator2 extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("high", "low")
    override val outputNames: Seq[String] = Seq("chaos")

    private[this] val n = 10
    private[this] val smoothLength = 60
    private[this] val rankLength = 252

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val hh = columnSet.select("high") | RunAgg(n, _.max) |-> "hh"
      val ll = columnSet.select("low") | RunAgg(n, _.min) |-> "ll"
      val sum = columnSet | function("high", "low")(_ - _) | RunSum(n) |-> "sum"

      (hh ++ ll ++ sum) |
        function("hh", "ll", "sum") { (hh, ll, sum) => sum / (hh - ll) } |
        SMA(smoothLength) |
        PercentRank(rankLength) |-> "chaos"
    }
  }

  case class ChaosIndicator(n: Int, smoothLength: Int, rankLength: Int) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("high", "low")
    override val outputNames: Seq[String] = Seq("chaos")

    override def apply(ds: DailyDataset): ColumnSet[K, T, V] = {
      ds | FractalDimension(10) | SMA(smoothLength) | PercentRank(rankLength) |-> "chaos"
    }
  }

  case class CongestionIndicator(n: Int, smoothLength: Int = 252) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("close")
    override val outputNames: Seq[String] = Seq("chaos")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val rocSum = columnSet | ROC(1) | function("roc")(abs) | RunSum(n) |-> "rocSum"
      val rocN = columnSet | ROC(n) | function("roc")(abs) |-> "rocN"

      rocN ++ rocSum | function("rocN", "rocSum")(_ / _) |-> "chaos"
    }
  }

  case class ER(n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("close")
    override val outputNames: Seq[String] = Seq("er")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val rocSum = columnSet | ROC(1) | function("roc")(abs) | RunSum(n) |-> "rocSum"
      val rocN = columnSet | ROC(n) | function("roc")(abs) |-> "rocN"

      rocN ++ rocSum | function("rocN", "rocSum")(_ / _) |-> "er"
    }
  }

  case class RunTTest1(n: Int, mu0 : Double = 0.0) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("x")
    override val outputNames: Seq[String] = Seq("tValue", "pValue")

    override def apply(ds: DailyDataset): DailyDataset = {
      val tDist = new TDistribution(null, n - 1)
      val mv = ds | MeanVariance(n)
      val tValue = mv | function("mean", "var") { (m, s2) => (m - mu0) / sqrt(s2 / n.toDouble) } |-> "tValue"

      val pValue = tValue | function("tValue") { t =>
        val absT = math.abs(t)

        2.0 * (1.0 - tDist.cumulativeProbability(absT)) // two-tailed test
      } |-> "pValue"

      tValue ++ pValue
    }
  }

  case class RunTTest(data1: String, data2: String, n: Int) extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(data1, data2)
    override val outputNames: Seq[String] = Seq("tValue", "pValue")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val Seq(s1, s2) = inputs
      val Seq(tValue, pValue) = outputs

      (0 until n - 1).foreach { i =>
        tValue(i) = Double.NaN
        pValue(i) = Double.NaN
      }

      (s1.values zip s2.values).sliding(n).zipWithIndex.foreach { case (window, i) =>
        val (v1, v2) = window.unzip
        val test = TTest(v1, v2)

        tValue(i + n - 1) = test.t
        pValue(i + n - 1) = test.pValue
      }
    }
  }


  case class IndicatorTTest(data: String, indicator: String, func: Double => Boolean, n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(data, indicator)
    override val outputNames: Seq[String] = Seq("pValue")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val d1 = columnSet | function(data, indicator) { (x, t) => if (func(t)) x else Double.NaN } |-> "d1"
      val d2 = columnSet | function(data, indicator) { (x, t) => if (!func(t)) x else Double.NaN } |-> "d2"

      (d1 ++ d2) | RunTTest("d1", "d2", n)
    }
  }

  case class RunCor(input1: String, input2: String, n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(input1, input2)
    override val outputNames: Seq[String] = Seq("cor")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val covXY = columnSet | RunCov(input1, input2, n) select "cov"
      val varX = columnSet.select(input1) | MeanVariance(n) |* "var" -> "varX"
      val varY = columnSet.select(input2) | MeanVariance(n) |* "var" -> "varY"

      (covXY ++ varX ++ varY) | function("cov", "varX", "varY") { (c, vx, vy) => c / sqrt(vx * vy) } |-> "cov"
    }
  }

  case class RunAutoCor(n: Int, lag: Int = 1) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("x")
    override val outputNames: Seq[String] = Seq("cor")

    override def apply(ds: DailyDataset): DailyDataset = {
      val a = ds |-> "a"
      val b = a | Lag(lag) |-> "b"

      a ++ b | RunCor("a", "b", n) |-> "cor"
    }
  }

  case class RunRange(n: Int) extends Calculation[K, T, V] {
    override val inputNames: Seq[String] = Seq("high", "low")
    override val outputNames: Seq[String] = Seq("r")

    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = {
      val hh = columnSet.select("high") | RunAgg(n, _.max) |-> "hh"
      val ll = columnSet.select("low") | RunAgg(n, _.min) |-> "ll"

      hh ++ ll | function("hh", "ll")(_ - _)
    }
  }

  case class FractalDimension(n: Int) extends DailyCalculation {
    require(n % 2 == 0, "[n] must be even")

    override val inputNames: Seq[String] = Seq("high", "low")
    override val outputNames: Seq[String] = Seq("fd")

    override def apply(ds: DailyDataset): DailyDataset = {
      val rangeN = ds | RunRange(n) |-> "rangeN"
      val rangeHalfN = ds | RunRange(n / 2)
      val range1 = rangeHalfN | Lag(n / 2) |-> "range1"
      val range2 = rangeHalfN |-> "range2"

      range1 ++ range2 ++ rangeN | function("range1", "range2", "rangeN") { (r1, r2, rN) =>
        ((log(r1 / (n / 2) + r2 / (n / 2)) - log(rN / n)) / log(2.0)) max 1.0 min 2.0
      }
    }
  }

  case class AdaptiveTrend(xName: String, vName: String, nMax: Int, func: Double => Int) extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(xName, vName)
    override val outputNames: Seq[String] = Seq("t")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val Seq(x, v) = inputs.map(_.values)
      val t = outputs.head

      t.indices.foreach { i =>
        if (i < nMax) {
          t(i) = Double.NaN
        }
        else {
          val n = func(v(i)) min nMax max 1
          val start = i - n + 1
          val end = i + 1
          val lm = LinearModel(x.slice(start, end) zip (start until end).map(_.toDouble))

          t(i) = lm.betaT
        }
      }
    }
  }

  case class FractalTrend(nMin: Int, nMax: Int, nFD: Int) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("high", "low", "close")
    override val outputNames: Seq[String] = Seq("t")

    override def apply(ds: DailyDataset): DailyDataset = {
      val x = ds |* "close" -> "x"
      val v = ds | FractalDimension(nFD) |-> "v"

      def func(fd: Double): Int = ((2.0 - fd) * (nMax - nMin) + nMin).round.toInt

      x ++ v | AdaptiveTrend("x", "v", nMax, func)
    }
  }

  case class PercentB(n: Int, mult: Double) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("close")
    override val outputNames: Seq[String] = Seq("pb")

    override def apply(ds: DailyDataset): DailyDataset = {
      val mv = ds | MeanVariance(n)

      ds ++ mv | function("close", "mean", "var") { (close, mean, variance) =>
        val sd = sqrt(variance)
        val top = mean + mult * sd
        val bottom = mean - mult * sd

        (close - bottom) / (top - bottom)
      }
    }
  }

  case class BollingerBands(n: Int, mult: Double) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("close")
    override val outputNames: Seq[String] = Seq("upper", "middle", "lower", "bandwidth", "percentB")

    override def apply(ds: DailyDataset): DailyDataset = {
      val mv = ds | MeanVariance(n)
      val upper = mv | function("mean", "var")(_ + mult * math.sqrt(_)) |-> "upper"
      val middle = mv |@ "mean" |-> "middle"
      val lower = mv | function("mean", "var")(_ - mult * math.sqrt(_)) |-> "lower"
      val bandwidth = upper ++ lower ++ middle | function("upper", "lower", "middle") { (u, l, m) => (u - l) / m } |-> "bandwidth"
      val percentB = ds ++ upper ++ lower | function("close", "upper", "lower") { (c, u, l) => (c - l) / (u - l) } |-> "percentB"

      upper ++ middle ++ lower ++ bandwidth ++ percentB
    }
  }

  // Write bollinger band

  case class Partition(data: String, indicator: String, p: Double => Boolean) extends DefaultCalculation[K, T, V] {
    override val inputNames: Seq[String] = Seq(data, indicator)
    override val outputNames: Seq[String] = Seq("dataIf", "dataElse")

    override def calculate(t: TimeSeriesTimeColumn[T], inputs: Seq[TimeSeriesDataColumn[V]], outputs: Seq[Array[V]]): Unit = {
      val Seq(dat, ind) = inputs
      val Seq(dataIf, dataElse) = outputs

      t.indices.foreach { i =>
        val t = dat(i)

        if (p(ind(i))) {
          dataIf(i) = t
        }
        else {
          dataElse(i) = t
        }
      }
    }
  }

  case class VolatilityRiskPremium(hv: String, iv: String, smoothLength: Int, dataLength: Int) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq(hv, iv)
    override val outputNames: Seq[String] = Seq("vrp")

    override def apply(ds: DailyDataset): DailyDataset = {
      val hvSmoothed = ds |@ hv |-> "hv"
      val ivSmoothed = ds |@ iv | SMA(smoothLength) |-> "iv"

      hvSmoothed ++ ivSmoothed | function("iv", "hv")(_ / _) | RunZ(dataLength) |-> "vrp"
    }
  }

  case class VolatilityTermStructure(near: String, far: String, smoothLength: Int, dataLength: Int) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq(near, far)
    override val outputNames: Seq[String] = Seq("vts")

    override def apply(ds: DailyDataset): DailyDataset = {
      val iv1 = ds |@ near | SMA(smoothLength) |-> "iv1"
      val iv2 = ds |@ far | SMA(smoothLength) |-> "iv2"

      iv1 ++ iv2 | function("iv2", "iv1")(_ / sqrt(_)) | RunZ(dataLength) |-> "vts"
    }
  }

  case class RunZ(n: Int, smoothLength: Int = 0) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("x")
    override val outputNames: Seq[String] = Seq("z")

    override def apply(ds: DailyDataset): DailyDataset = {
      val x = ds |-> "x"
      val mv = x | MeanVariance(n)
      val t = (if (smoothLength > 0) x | SMA(smoothLength) else x) |-> "t"

      t ++ mv | function("t", "mean", "var") { (t, m, v) => (t - m) / sqrt(v) } |-> "z"
    }
  }

  case class StochOsc(n: Int) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("high", "low", "close")
    override val outputNames: Seq[String] = Seq("stoch")

    override def apply(ds: DailyDataset): DailyDataset = {
      val x = ds |* "close" -> "x"
      val max = ds |@ "high" | RunAgg(n, _.max) |-> "max"
      val min = ds |@ "low" | RunAgg(n, _.min) |-> "min"

      x ++ max ++ min | function("x", "max", "min") { (t, hh, ll) =>
        if (hh > ll) (t - ll) / (hh - ll) else Double.NaN
      } |-> "stoch"
    }
  }

  case class Select[K, T, V](names: String*) extends Calculation[K, T, V] {
    override def inputNames: Seq[String] = names
    override def outputNames: Seq[String] = names
    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = columnSet.select(names: _*)
  }

  case class SelectAs[K, T, V](m: (String, String)*) extends Calculation[K, T, V] {
    override def inputNames: Seq[String] = m.unzip._1
    override def outputNames: Seq[String] = m.unzip._2
    override def apply(columnSet: ColumnSet[K, T, V]): ColumnSet[K, T, V] = columnSet.selectAs(m)
  }

  case class ValidityMask(data: String, dataLength: Int, recentInvalid: Int, percentInvalid: Double) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq(data)
    override val outputNames: Seq[String] = Seq("mask")

    override def apply(ds: DailyDataset): DailyDataset = {
      val x = ds | function(data) { t => if (t.isNaN) 1 else 0 }
      val sum1 = x | RunSum(recentInvalid) |-> "sum1"
      val sum2 = x | RunSum(dataLength) |-> "sum2"
      val threshold = dataLength * percentInvalid;

      sum1 ++ sum2 | function("sum1", "sum2") { (s1, s2) =>
        if (s1.isNaN || s2.isNaN || s1 > 0 || s2 > threshold) {
          Double.NaN
        }
        else {
          1.0
        }
      } |-> "mask"
    }
  }

  case class ApplyMask(mask: DailyDataset) extends DailyCalculation {
    override val inputNames: Seq[String] = Seq("data")
    override val outputNames: Seq[String] = Seq("data")
    override def apply(ds: DailyDataset): DailyDataset = {
      val d = ds |-> "data"
      val m = mask |-> "mask"

      d ++ m | function("data", "mask")(_ * _)
    }
  }
}
