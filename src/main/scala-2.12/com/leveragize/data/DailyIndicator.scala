package com.leveragize.data

import java.time.LocalDate
import java.util.UUID

import com.leveragize.data.sql.TimeSeriesQuery
import com.leveragize.data.sql._
import scalikejdbc._
import jsr310._

/**
  * Created by peterho on 4/19/17.
  */
object DailyIndicator extends HasFields {
  val allDataFields = Seq("avgVol" -> sqls"avg_vol", "roc252" -> sqls"roc252",
    "night21" -> sqls"night21", "day21" -> sqls"day21",
    "mov21" -> sqls"mov21", "mov63" -> sqls"mov63", "mov252" -> sqls"mov252",
    "iv30Rank" -> sqls"iv30_rank", "iv90Rank" -> sqls"iv90_rank",
    "iv30Stoch" -> sqls"iv30_stoch", "iv90Stoch" -> sqls"iv90_stoch",
    "iv30S" -> sqls"iv30_smoothed", "iv90S" -> sqls"iv90_smoothed", "hv" -> sqls"hv", "hvRank" -> sqls"hv_rank",
    "er21" -> sqls"er21", "er63" -> sqls"er63", "ma20" -> sqls"ma20", "ma50" -> sqls"ma50", "ma200" -> sqls"ma200",
    "stoch63" -> sqls"stoch63", "stoch252" -> sqls"stoch252", "rsi5" -> sqls"rsi5", "rsi10" -> sqls"rsi10",
    "retCO" -> sqls"ret_co", "retOC" -> sqls"ret_oc", "ret21" -> sqls"ret21"
  )

  val query: DailyDatasetQuery = TimeSeriesQuery[UUID, LocalDate, Double](
    sqls"public", sqls"daily_indicator", sqls"sym_id", sqls"date", allDataFields: _*)
}
