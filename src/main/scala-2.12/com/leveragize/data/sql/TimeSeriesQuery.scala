package com.leveragize.data.sql

import com.leveragize.data._
import scalikejdbc._

/**
  * Created by peterho on 1/22/17.
  */
case class TimeSeriesQueryAdapter(schema: SQLSyntax, table: SQLSyntax, datasetKey: SQLSyntax,
                                  time: SQLSyntax, values: Map[String, SQLSyntax])

class TimeSeriesQuery[K, T, V](metadata: TimeSeriesDatasetMetadata, adapter: TimeSeriesQueryAdapter)
                              (implicit ev: TimeTypeRelated[T], pbfK: ParameterBinderFactory[K], pbfT: ParameterBinderFactory[T],
                               tbK: TypeBinder[K], tbT: TypeBinder[T], tbV: TypeBinder[V], defaults: DataDefaults[V]) {

  def getDataset(key: K, timeRange: RangeCondition[T] = RangeCondition[T]())
                (implicit rcr: SQLRangeConditionRelated[T], s: DBSession): Option[ColumnSet[K, T, V]] = {

    getDatasets(Seq(key), timeRange).lift(key)
  }

  def getDatasets(keys: Seq[K], timeRange: RangeCondition[T] = RangeCondition[T]())
                 (implicit rcr: SQLRangeConditionRelated[T], s: DBSession): Map[K, ColumnSet[K, T, V]] = {

    val values = metadata.columnNames.map(adapter.values)
    val list = SQLSyntax.join(values, sqls",", spaceBeforeDelimier = false)
    val conditions = timeRange.translate(adapter.time).foldLeft(sqls"${adapter.datasetKey} IN (${keys.map(pbfK.apply)})")(_ and _)
    val builder = new TimeSeriesDatasetBuilder[K, T, V](metadata)
    val nullValue = defaults.nullValue

    sql"""
      |SELECT ${adapter.datasetKey}, ${adapter.time}, $list FROM ${adapter.schema}.${adapter.table}
      |WHERE $conditions ORDER BY ${adapter.time} ASC
      """
      .stripMargin
      .foreach { rs =>
        builder += TimeSeriesData(rs.get[K](1), rs.get[T](2),
          (3 until 3 + values.length).map(i => rs.get[Option[V]](i).getOrElse(nullValue)))
      }

    builder.result
  }

  def saveDatasets(datasets: Iterable[ColumnSet[K, T, V]])(implicit session: DBSession): Unit = {
    val columnNames = metadata.columnNames
    val sqlNames = SQLSyntax.join(columnNames.map(adapter.values), sqls",", spaceBeforeDelimier = false)
    val placeholders = SQLSyntax.join(columnNames.map(_ => sqls"?"), sqls",", spaceBeforeDelimier = false)
    val conflictUpdates = SQLSyntax.join(columnNames.map(adapter.values).map { s => sqls"$s = EXCLUDED.$s" }, sqls",", spaceBeforeDelimier = false)

    val sql =
      sql"""
        |INSERT INTO ${adapter.schema}.${adapter.table} (${adapter.datasetKey}, ${adapter.time}, $sqlNames)
        |VALUES (?, ?, $placeholders)
        |ON CONFLICT (${adapter.datasetKey}, ${adapter.time}) DO UPDATE SET $conflictUpdates
        """
        .stripMargin

    datasets.foreach { ds =>
      val timeValues = ds.timeColumn.values
      val dataValues = columnNames.map(ds.dataColumn(_).values).toList

      val batchParams = timeValues.zipWithIndex.map { case (t, i) =>
        pbfK(ds.datasetKey) :: pbfT(t) :: dataValues.map(col => defaults.makeOption(col(i)))
      }

      sql.batch(batchParams: _*).apply()
    }
  }
}

object TimeSeriesQuery {
  def apply[K, T, V](schema: SQLSyntax, table: SQLSyntax, datasetKey: SQLSyntax, time: SQLSyntax, data: (String, SQLSyntax)*)
                    (implicit ev: TimeTypeRelated[T], pbfK: ParameterBinderFactory[K], pbfT: ParameterBinderFactory[T],
                     tbK: TypeBinder[K], tbT: TypeBinder[T], tbV: TypeBinder[V], defaults: DataDefaults[V]): TimeSeriesQuery[K, T, V] = {

    val a = TimeSeriesQueryAdapter(schema, table, datasetKey, time, data.toMap)
    val m = TimeSeriesDatasetMetadata(data.map(_._1).toVector)

    new TimeSeriesQuery[K, T, V](m, a)
  }
}