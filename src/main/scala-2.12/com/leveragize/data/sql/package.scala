package com.leveragize.data

import java.sql.ResultSet
import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import scalikejdbc._

/**
  * Created by peterho on 1/22/17.
  */
package object sql {
  implicit val uuidBinder = new Binders[UUID] {
    def apply(rs: ResultSet, columnIndex: Int): UUID = UUID.fromString(rs.getString(columnIndex))
    def apply(rs: ResultSet, columnLabel: String): UUID = UUID.fromString(rs.getString(columnLabel))
    def apply(value: UUID): ParameterBinderWithValue = ParameterBinder(value, (ps, i) => ps.setObject(i, value))
  }

  abstract class SQLRangeConditionRelated[T](implicit pbf: ParameterBinderFactory[T])
    extends RangeConditionRelated[SQLSyntax, T, SQLSyntax] {

    def lt(left: SQLSyntax, right: T): SQLSyntax = sqls"$left < ${pbf(right)}"
    def gt(left: SQLSyntax, right: T): SQLSyntax = sqls"$left > ${pbf(right)}"
    def lte(left: SQLSyntax, right: T): SQLSyntax = sqls"$left <= ${pbf(right)}"
    def gte(left: SQLSyntax, right: T): SQLSyntax = sqls"$left >= ${pbf(right)}"
  }

  object SQLRangeConditionRelated {
    implicit object intRangeCondition extends SQLRangeConditionRelated[Int]
    implicit object localDateRangeCondition extends SQLRangeConditionRelated[LocalDate]
    implicit object localDateTimeRangeCondition extends SQLRangeConditionRelated[LocalDateTime]
  }
}
