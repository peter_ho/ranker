package com.leveragize.common

/**
  * Created by peterho on 1/16/17.
  */

trait TimeUnit {
  def multiplier: Long
  def name: String

  def convertFromNanoSeconds(ns: Double): Double = ns / multiplier
  def concertToNanoSeconds(x: Double): Double = x * multiplier
  def formatValue(x: Double): String = f"$x%1.5f $name"
}

case object Second extends TimeUnit {
  val multiplier = 1000000000L
  val name = "s"
}

case object Millisecond extends TimeUnit {
  val multiplier = 1000000L
  val name = "ms"
}

case object Nanosecond extends TimeUnit {
  val multiplier = 1L
  val name = "ns"
}


object StopWatch {
  def time[T](block: => T)(implicit unit: TimeUnit = Millisecond): (T, Double) = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()

    (result, unit.convertFromNanoSeconds(t1 - t0))
  }

  def printElapsedTime[T](message: String = "Elapsed Time")(block: => T)(implicit unit: TimeUnit = Millisecond): T = {
    val (result, t) = time(block)

    println(s"$message: ${unit.formatValue(t)}")
    result
  }
}
