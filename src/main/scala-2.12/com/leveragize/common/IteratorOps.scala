package com.leveragize.common

/**
  * Created by peterho on 1/16/17.
  */
object IteratorOps {
  implicit class IteratorOps[T](val iterator: Iterator[T]) {
    def nextOption(): Option[T] = if (iterator.hasNext) Some(iterator.next()) else None
  }
}