package com.leveragize.stat

/**
  * Created by peterho on 2/2/17.
  */
case class LinearModel(pairs: Seq[(Double, Double)]) {
  private[this] val (x, y) = pairs.unzip
  private[this] val n = x.length

  require(y.lengthCompare(n) == 0, "Lengths must match")

  private[this] val covXY = covariance(x, y)
  private[this] val varX = variance(x)
  private[this] val varY = variance(y)
  private[this] val meanX = mean(x)

  val beta: Double = covXY / varX

  lazy val alpha: Double = mean(y) - beta * meanX

  lazy val r2: Double = {
    val x2 = x map sq
    val y2 = y map sq
    val res = 1.0 / (n * (n - 2.0)) * (n * y2.sum - sq(y.sum) - sq(beta) * (n * x2.sum - sq(x.sum)))

    1.0 - res / (varY * (n - 1.0) / (n - 2.0))
  }

  lazy val errResidual: Double = math.sqrt(varY * (1.0 - r2) * (n - 1.0) / (n - 2.0))

  def errAlpha: Double = errResidual / math.sqrt(n) * math.sqrt(1.0 + sq(meanX) / (varX * (n - 1.0) / n))
  def errBeta: Double = errResidual / math.sqrt(varX * (n - 1.0))
  def alphaT: Double = alpha / errAlpha
  def betaT: Double = beta / errBeta
}
