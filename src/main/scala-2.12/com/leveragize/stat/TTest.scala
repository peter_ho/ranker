package com.leveragize.stat

import org.apache.commons.math3.distribution.TDistribution

import scala.math.sqrt

/**
  * Created by peterho on 3/10/17.
  */
trait TTest {
  def t: Double
  def v: Double

  def pValue: Double = {
    val tDist = new TDistribution(null, v)
    val absT = math.abs(t)

    1.0 - tDist.cumulativeProbability(absT) + tDist.cumulativeProbability(-absT)
  }
}

object TTest {
  def apply(s1: Seq[Double], s2: Seq[Double]): TTest = new WelchTTest(s1, s2)
  def apply(s: Seq[Double]): TTest = new OneSampleTTest(s: Seq[Double])
  def paired(p: Seq[(Double, Double)]) = new OneSampleTTest(p map { case (a, b) => a - b })
}

class WelchTTest(s1: Seq[Double], s2: Seq[Double]) extends TTest {
  private[this] val (n1, mean1, var1) = countMeanVariance(s1)
  private[this] val (n2, mean2, var2) = countMeanVariance(s2)

  val t: Double = (mean1 - mean2) / math.sqrt(var1 / n1 + var2 / n2)

  // Welch–Satterthwaite equation
  lazy val v: Double = sq(var1 / n1 + var2 / n2) / (sq(var1) / (sq(n1) * (n1 - 1)) + sq(var2) / (sq(n2) * (n2 - 1)))
}

class OneSampleTTest(s: Seq[Double]) extends TTest {
  private[this] val (n, mean, s2) = countMeanVariance(s)

  lazy val t: Double = mean / sqrt(s2 / n.toDouble)
  lazy val v: Double = n - 1
}
