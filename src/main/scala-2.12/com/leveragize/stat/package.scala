package com.leveragize

/**
  * Created by peterho on 1/31/17.
  */
package object stat {
  def mean(a: Seq[Double]): Double = {
    var n = 0
    var sum = 0.0

    a.foreach { x =>
      if (!x.isNaN) {
        n += 1
        sum += x
      }
    }

    sum / n
  }

  def countMeanVariance(a: Seq[Double]): (Int, Double, Double) = {
    var mean = 0.0
    var m2 = 0.0
    var n = 0

    a.foreach { x =>
      if (!x.isNaN) {
        n += 1
        val delta1 = x - mean
        mean += delta1 / n
        val delta2 = x - mean
        m2 += delta1 * delta2
      }
    }

    (n, if (n > 0) mean else Double.NaN, if (n > 1) m2 / (n - 1.0) else Double.NaN)
  }

  def variance(a: Seq[Double]): Double = countMeanVariance(a)._3

  def covariance(a: Seq[Double], b: Seq[Double]): Double = {
    covariance(a zip b)
  }

  def covariance(pairs: Seq[(Double, Double)]): Double = {
    var mean1 = 0.0
    var mean2 = 0.0
    var m12 = 0.0
    var n = 0

    pairs foreach { case (x, y) =>
      if (!x.isNaN && !y.isNaN) {
        n += 1
        val delta1 = (x - mean1) / n
        mean1 += delta1
        val delta2 = (y - mean2) / n
        mean2 += delta2
        m12 += (n - 1) * delta1 * delta2 - m12 / n
      }
    }

    n * m12 / (n - 1.0)
  }

  def correlation(a: Seq[Double], b: Seq[Double]): Double = {
    covariance(a, b) / math.sqrt(variance(a) * variance(b))
  }

  def correlation(pairs: Seq[(Double, Double)]): Double = {
    val (a, b) = pairs.unzip

    correlation(a, b)
  }

  def sq(x: Double): Double = x * x

  def quantile(array: IndexedSeq[Double], p: Double, sorted: Boolean = false): Double = {
    // Type 8 quantile
    val v = if (sorted) array else array.sorted
    val n = v.length

    if (n < 2) {
      Double.NaN
    }
    else if (p < (2.0 / 3.0) / (n + 1.0 / 3.0)) {
      v(0)
    }
    else if (p >= (n - 1.0 / 3.0) / (n + 1.0 / 3.0)) {
      v(n - 1)
    }
    else {
      val h = (n + 1.0 / 3.0) * p + 1.0 / 3.0
      val hF = math.floor(h)
      val i = hF.toInt - 1

      v(i) + (h - hF) * (v(i + 1) - v(i))
    }
  }
}
