package com.leveragize.stat

import scala.math.log

/**
  * Created by peterho on 6/6/17.
  */
case class VarianceRatioTest(x: IndexedSeq[Double], diff: (Double, Double) => Double = _ - _) {
  private[this] val arr = x.filterNot(_.isNaN)

  private[this] def sq(value: Double) = value * value

  private[this] def calcVar(a: IndexedSeq[Double], k: Int, q: Int) = {
    val n = k * q
    val factor = k / ((n - q + 1.0) * (k - 1.0))
    val mu = diff(a(n), a.head) / k

    factor * (q to n).foldLeft(0.0)((sum, j) => sum + sq(diff(a(j), a(j - q)) - mu))
  }

  private[this] def hurst(qList: Seq[Int]) = {
    val qMax = qList.last
    val n = (arr.length - 1) / qMax * qMax

    if (n > 0) {
      val logPeriods = qList.map(q => log(q))
      val logVars = qList.map(q => log(calcVar(arr.slice(arr.length - n - 1, arr.length), n / q, q)))

      LinearModel(logPeriods zip logVars).beta / 2.0
    }
    else {
      Double.NaN
    }
  }

  def hurst(n: Int): Double = hurst(Seq(1, n))
  def hurst: Double = hurst((0 to 7) map (1 << _))
}
