--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: daily_data; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE daily_data (
    sym_id uuid NOT NULL,
    date date NOT NULL,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume double precision,
    skew30 double precision,
    iv10c double precision,
    iv10p double precision,
    iv30c double precision,
    iv30p double precision,
    iv90c double precision,
    iv90p double precision
);


ALTER TABLE daily_data OWNER TO peterho;

--
-- Name: daily_indicator; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE daily_indicator (
    sym_id uuid NOT NULL,
    date date NOT NULL,
    avg_vol double precision,
    roc252 double precision,
    night21 double precision,
    day21 double precision,
    mov21 double precision,
    mov63 double precision,
    mov252 double precision,
    iv30_rank double precision,
    iv90_rank double precision,
    iv30_stoch double precision,
    iv90_stoch double precision,
    iv30_smoothed double precision,
    iv90_smoothed double precision,
    hv double precision,
    hv_rank double precision,
    er21 double precision,
    er63 double precision,
    ma50 double precision,
    ma200 double precision,
    rsi5 double precision,
    rsi10 double precision,
    stoch63 double precision,
    stoch252 double precision,
    ret_co double precision,
    ret_oc double precision,
    ma20 double precision,
    ret21 double precision
);


ALTER TABLE daily_indicator OWNER TO peterho;

--
-- Name: dataset_table; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE dataset_table (
    database_code text,
    dataset_code text,
    name text,
    "timestamp" timestamp with time zone DEFAULT now(),
    id uuid DEFAULT uuid_generate_v4() NOT NULL
);


ALTER TABLE dataset_table OWNER TO peterho;

--
-- Name: eod_input; Type: TABLE; Schema: public; Owner: peterho
--

CREATE UNLOGGED TABLE eod_input (
    sym_id uuid,
    date date,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume double precision
);


ALTER TABLE eod_input OWNER TO peterho;

--
-- Name: lifetime_stats; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE lifetime_stats (
    sym_id uuid NOT NULL,
    count_m30 integer,
    count_m20 integer,
    count_m10 integer,
    count_m05 integer,
    count_p05 integer,
    count_p10 integer,
    count_p20 integer,
    count_p30 integer,
    n integer,
    start_date date,
    null_pct double precision,
    put_ev double precision,
    call_ev double precision
);


ALTER TABLE lifetime_stats OWNER TO peterho;

--
-- Name: load_table; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE load_table (
    sym_id uuid NOT NULL,
    last_updated timestamp with time zone,
    process_id integer
);


ALTER TABLE load_table OWNER TO peterho;

--
-- Name: sym_table; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE sym_table (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name text,
    symbol text NOT NULL,
    price_dataset_id uuid,
    vol_dataset_id uuid
);


ALTER TABLE sym_table OWNER TO peterho;

--
-- Name: prob_view; Type: VIEW; Schema: public; Owner: peterho
--

CREATE VIEW prob_view AS
 SELECT sym_table.id AS sym_id,
    sym_table.symbol,
    lifetime_stats.start_date,
    lifetime_stats.null_pct,
    lifetime_stats.call_ev,
    lifetime_stats.put_ev,
    ((lifetime_stats.count_m20)::double precision / (lifetime_stats.n)::double precision) AS prob_m20,
    ((lifetime_stats.count_p20)::double precision / (lifetime_stats.n)::double precision) AS prob_p20
   FROM (lifetime_stats
     JOIN sym_table ON ((lifetime_stats.sym_id = sym_table.id)))
  WHERE (lifetime_stats.n > 0);


ALTER TABLE prob_view OWNER TO peterho;

--
-- Name: summary_load_table; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE summary_load_table (
    sym_id uuid NOT NULL,
    last_updated timestamp with time zone,
    process_id integer
);


ALTER TABLE summary_load_table OWNER TO peterho;

--
-- Name: summary_table; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE summary_table (
    sym_id uuid NOT NULL,
    symbol text,
    name text,
    ret_co_oc double precision,
    ret_ma20 double precision,
    ret_ma50 double precision,
    ret_ma200 double precision,
    ret_avg double precision,
    call double precision,
    put double precision,
    call_mov21_hi double precision,
    call_mov21_lo double precision,
    put_mov21_hi double precision,
    put_mov21_lo double precision,
    call_er21_hi double precision,
    call_er21_lo double precision,
    put_er21_hi double precision,
    put_er21_lo double precision,
    call_rsi10_hi double precision,
    call_rsi10_lo double precision,
    put_rsi10_hi double precision,
    put_rsi10_lo double precision,
    call_iv30_iv90_hi double precision,
    call_iv30_iv90_lo double precision,
    put_iv30_iv90_hi double precision,
    put_iv30_iv90_lo double precision,
    call_iv30_hv_hi double precision,
    call_iv30_hv_lo double precision,
    put_iv30_hv_hi double precision,
    put_iv30_hv_lo double precision,
    length double precision,
    null_pct double precision
);


ALTER TABLE summary_table OWNER TO peterho;

--
-- Name: test; Type: TABLE; Schema: public; Owner: peterho
--

CREATE TABLE test (
    id integer NOT NULL,
    date date
);


ALTER TABLE test OWNER TO peterho;

--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: peterho
--

CREATE SEQUENCE test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_id_seq OWNER TO peterho;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: peterho
--

ALTER SEQUENCE test_id_seq OWNED BY test.id;


--
-- Name: vol_input; Type: TABLE; Schema: public; Owner: peterho
--

CREATE UNLOGGED TABLE vol_input (
    sym_id uuid,
    date date,
    skew30 double precision,
    iv10c double precision,
    iv10p double precision,
    iv30c double precision,
    iv30p double precision,
    iv90c double precision,
    iv90p double precision
);


ALTER TABLE vol_input OWNER TO peterho;

--
-- Name: test id; Type: DEFAULT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_id_seq'::regclass);


--
-- Name: daily_data daily_data_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY daily_data
    ADD CONSTRAINT daily_data_pkey PRIMARY KEY (sym_id, date);


--
-- Name: daily_indicator daily_indicator_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY daily_indicator
    ADD CONSTRAINT daily_indicator_pkey PRIMARY KEY (sym_id, date);


--
-- Name: dataset_table dataset_table_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY dataset_table
    ADD CONSTRAINT dataset_table_pkey PRIMARY KEY (id);


--
-- Name: lifetime_stats lifetime_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY lifetime_stats
    ADD CONSTRAINT lifetime_stats_pkey PRIMARY KEY (sym_id);


--
-- Name: load_table load_table_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY load_table
    ADD CONSTRAINT load_table_pkey PRIMARY KEY (sym_id);


--
-- Name: summary_load_table summary_load_table_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY summary_load_table
    ADD CONSTRAINT summary_load_table_pkey PRIMARY KEY (sym_id);


--
-- Name: summary_table summary_table_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY summary_table
    ADD CONSTRAINT summary_table_pkey PRIMARY KEY (sym_id);


--
-- Name: sym_table sym_table_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY sym_table
    ADD CONSTRAINT sym_table_pkey PRIMARY KEY (id);


--
-- Name: sym_table sym_table_price_dataset_id_key; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY sym_table
    ADD CONSTRAINT sym_table_price_dataset_id_key UNIQUE (price_dataset_id);


--
-- Name: sym_table sym_table_symbol_key; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY sym_table
    ADD CONSTRAINT sym_table_symbol_key UNIQUE (symbol);


--
-- Name: sym_table sym_table_vol_dataset_id_key; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY sym_table
    ADD CONSTRAINT sym_table_vol_dataset_id_key UNIQUE (vol_dataset_id);


--
-- Name: test test_pkey; Type: CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY test
    ADD CONSTRAINT test_pkey PRIMARY KEY (id);


--
-- Name: daily_data_date; Type: INDEX; Schema: public; Owner: peterho
--

CREATE INDEX daily_data_date ON daily_data USING btree (date);


--
-- Name: daily_indicator_date; Type: INDEX; Schema: public; Owner: peterho
--

CREATE INDEX daily_indicator_date ON daily_indicator USING btree (date);


--
-- Name: dataset_table_codes; Type: INDEX; Schema: public; Owner: peterho
--

CREATE UNIQUE INDEX dataset_table_codes ON dataset_table USING btree (database_code, dataset_code);


--
-- Name: load_table_process_id; Type: INDEX; Schema: public; Owner: peterho
--

CREATE INDEX load_table_process_id ON load_table USING btree (process_id);


--
-- Name: summary_load_table_process_id; Type: INDEX; Schema: public; Owner: peterho
--

CREATE INDEX summary_load_table_process_id ON summary_load_table USING btree (process_id);


--
-- Name: sym_table sym_table_price_dataset_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY sym_table
    ADD CONSTRAINT sym_table_price_dataset_id_fkey FOREIGN KEY (price_dataset_id) REFERENCES dataset_table(id) ON DELETE SET NULL;


--
-- Name: sym_table sym_table_vol_dataset_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: peterho
--

ALTER TABLE ONLY sym_table
    ADD CONSTRAINT sym_table_vol_dataset_id_fkey FOREIGN KEY (vol_dataset_id) REFERENCES dataset_table(id) ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

CREATE MATERIALIZED VIEW snapshot_table AS
SELECT dd.close, dd.volume, (dd.iv30c + dd.iv90p) / 2 AS iv30, di.*, di.iv30_smoothed / di.iv90_smoothed AS iv30_iv90, di.iv30_smoothed / di.hv AS iv30_hv
FROM daily_data dd INNER JOIN daily_indicator di ON dd.sym_id = di.sym_id AND dd.date = di.date AND dd.date > (date(now()) - 60);

ALTER MATERIALIZED VIEW snapshot_table OWNER TO peterho;

CREATE INDEX snapshot_table_date ON snapshot_table USING btree (date);
