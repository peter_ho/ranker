INSERT INTO sym_table (symbol, price_dataset_id, vol_dataset_id)
(SELECT p.dataset_code, p.id, v.id
FROM dataset_table v LEFT OUTER JOIN dataset_table p ON p.database_code = 'EOD' AND v.dataset_code = p.dataset_code
WHERE v.database_code = ‘VOL’ AND p.dataset_code IS NOT NULL)
ON CONFLICT (symbol) DO NOTHING

-- truncate eod_input
-- truncate vol_input
-- truncate sym_table




INSERT INTO daily_data (sym_id, date, open, high, low, close, volume)
(SELECT sym_id, date, open, high, low, close, volume FROM eod_input)
ON CONFLICT ON CONSTRAINT daily_data_pkey DO
UPDATE SET open = EXCLUDED.open, high = EXCLUDED.high, low = EXCLUDED.low, close = EXCLUDED.close, volume = EXCLUDED.volume

UPDATE daily_data
SET iv10c = x.iv10c, iv10p = x.iv10p, iv30c = x.iv30c, iv30p = x.iv30p, iv90c = x.iv90c, iv90p = x.iv90p, skew30 = x.skew30
FROM vol_input x WHERE daily_data.sym_id = x.sym_id AND daily_data.date = x.date


-- truncate daily_data
-- truncate daily_indicator

UPDATE summary_table SET symbol = sym_table.symbol, name = sym_table.name
FROM sym_table WHERE summary_table.sym_id = sym_table.id




INSERT INTO summary_load_table(sym_id)
(SELECT id FROM sym_table)

--truncate summary_table


update load_table set process_id = null where process_id is null

UPDATE summary_load_table SET process_id = null

UPDATE summary_load_table lt
SET process_id = 1
FROM sym_table
WHERE lt.sym_id = sym_table.id
AND symbol in ('SPY', 'SDS', 'SSO', 'OEF', 'VXX', 'UVXY', 'MO', 'DIA', 'AAPL', 'CSIQ')

select sym_table.symbol, summary_table.* from summary_table inner join sym_table on sym_table.id = summary_table.sym_id
where length > 8 and null_pct < 0.05 and ret_avg > 0

select sym_table.symbol, put_iv30_rank_hi + call_iv30_rank_hi, summary_table.* from summary_table inner join sym_table on sym_table.id = summary_table.sym_id
where length > 8 and null_pct < 0.05 and call_iv30_rank_hi + put_iv30_rank_hi - put - call < 0

select sym_table.symbol, call_hv_rank_hi + put_hv_rank_hi, summary_table.* from summary_table inner join sym_table on sym_table.id = summary_table.sym_id
where length > 8 and null_pct < 0.05 and call_hv_rank_hi + put_hv_rank_hi - call - put < 0

select sym_table.symbol, call_iv30_rank_hi + put_iv30_rank_hi - call - put, summary_table.* from summary_table inner join sym_table on sym_table.id = summary_table.sym_id
where length > 8 and null_pct < 0.05 and ret_avg > 0


UPDATE load_table lt
SET process_id = 1
FROM sym_table
WHERE lt.sym_id = sym_table.id
AND symbol in ('SPY', 'SDS', 'SSO', 'OEF', 'VXX', 'UVXY', 'MO', 'DIA', 'AAPL')