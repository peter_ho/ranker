import java.time.LocalDate
import java.util.UUID

import com.leveragize.data.DailyIndicators._
import com.leveragize.data.{ChainedCalculations, ColumnSet, ColumnSetWithNew, DefaultColumnSet}
import com.leveragize.stat._
import org.scalactic.{Equality, TolerantNumerics}
import org.scalatest.FunSuite

import scala.math._

/**
  * Created by peterho on 1/25/17.
  */
class CalculationSuite extends FunSuite {
  implicit val doubleEquality: Equality[Double] = TolerantNumerics.tolerantDoubleEquality(0.0001)

  private val uuid = UUID.randomUUID()

  private val t = (1 to 10).map(LocalDate.of(2010, 1, _)).toVector
  private val open = Vector(1.3, 1.1, 1.3, 1.2, 1.8, 1.4, 2.2, 2.1, 2.4, 2.3)
  private val high = Vector(1.31, 1.32, 1.33, 1.44, 1.85, 1.66, 2.22, 2.33, 2.44, 2.55)
  private val low = Vector(1.0, 0.9, 1.05, 1.12, 1.53, 1.33, 1.99, 2.0, 2.11, 2.22)
  private val close = Vector(1.2, 1.2, 1.2, 1.3, 1.7, 1.5, 2.1, 2.2, 2.3, 2.4)
  private val sigma = Vector(0.01, 0.02, 0.03, 0.04, 0.03, 0.02, 0.01, 0.02, 0.03, 0.04)
  private val bad = close.updated(0, Double.NaN)
  private val bad2 = Vector(Double.NaN, 1.2, 1.2, 1.3, Double.NaN, Double.NaN, 2.1, 2.2, 2.3, Double.NaN)

  private val columnSet: ColumnSet[UUID, LocalDate, Double] with ColumnSetWithNew[UUID, LocalDate, Double] = {
    DefaultColumnSet(uuid, t, Seq(("open", open), ("high", high), ("low", low), ("close", close), ("sigma", sigma), ("bad", bad), ("bad2", bad2)))
  }

  val ohlc = Seq("open", "high", "low", "close")

  def assertSeqEqual(a: IndexedSeq[Double], b: IndexedSeq[Double]): Unit = {
    assert(a.lengthCompare(b.length) == 0 && (a zip b).forall { case (x, y) => x.isNaN && y.isNaN || x === y })
  }

  test("mean") {
    assert(mean(close) === 1.71)
  }

  test("Variance") {
    assert(variance(close) === 0.2454444)
  }

  test("Covariance") {
    assert(covariance(high, low) === 0.2551722)
  }

  test("Correlation") {
    assert(correlation(high, low) === 0.9958984)
  }

  test("t-Test") {
    val test = TTest(high, low)

    assert(test.t === 1.4134)
    assert(test.v === 17.99)
    assert(test.pValue === 0.1746)
  }

  test("Linear Regression") {
    val lm = LinearModel(high zip low)

    assert(lm.alpha === -0.3562897)
    assert(lm.beta === 1.019669)
    assert(lm.r2 === 0.9918135)
    assert(lm.errAlpha === 0.06240)
    assert(lm.errBeta === 0.03275)
    assert(lm.alphaT === -5.7101)
    assert(lm.betaT === 31.1323)
    assert(lm.errResidual === 0.04915365)
  }

  test("Forward Fill") {
    val ff = columnSet | ForwardFill("bad2")

    assertResult(1)(ff.columnCount)

    Vector(Double.NaN, 1.2, 1.2, 1.3, 1.3, 1.3, 2.1, 2.2, 2.3, 2.3) zip ff.dataColumn("bad2").values foreach { case (a, b) =>
      assert(a.isNaN && b.isNaN || a === b)
    }
  }

  test("SMA") {
    val sma = columnSet.select("close") | SMA(4)
    val values = sma.firstDataColumn.values

    (0 to 2).foreach { i => assertResult(true)(values(i).isNaN) }
    Vector(1.225, 1.350, 1.425, 1.650, 1.875, 2.025) zip (3 to 9) foreach { case (a, b) => assert(a === values(b)) }
  }

  test("SMA with NaN") {
    val sma = columnSet.select("bad") | SMA(4)
    val values = sma.firstDataColumn.values

    (0 to 3).foreach { i => assertResult(true)(values(i).isNaN) }
    Vector(1.350, 1.425, 1.650, 1.875, 2.025) zip (4 to 9) foreach { case (a, b) => assert(a === values(b)) }
  }

  test("RSI") {
    val rsi = columnSet | RSI(3)
    val values = rsi.firstDataColumn.values

    (0 to 2).foreach { i => assertResult(true)(values(i).isNaN) }
    (3 to 4).foreach { i => assert(100.0 === values(i)) }
    assert(71.42857 === values(5))
    assert(83.33333 === values(6))
    assert(77.77778 === values(7))
    (8 to 9).foreach { i => assert(100.0 === values(i)) }
  }

  test("RSI with NaN") {
    val rsi = columnSet.select("bad") | RSI(3)
    val values = rsi.firstDataColumn.values

    (0 to 3).foreach { i => assertResult(true)(values(i).isNaN) }
    assert(100.0 === values(4))
    assert(71.42857 === values(5))
  }

  test("PercentRank") {
    val pr = columnSet.select("close") | PercentRank(3)
    val values = pr.firstDataColumn.values

    (0 to 1).foreach { i => assertResult(true)(values(i).isNaN) }
    assert(0.5 === values(2))
    (3 to 4).foreach { i => assert(0.8333333 === values(i)) }
    assert(0.5 === values(5))
    (6 to 9).foreach { i => assert(0.8333333 === values(i)) }
  }

  test("RSI then PercentRank") {
    val rsi = columnSet.select("close") | RSI(3) | PercentRank(3)
    val values = rsi.firstDataColumn.values

    (0 to 4).foreach { i => assertResult(true)(values(i).isNaN) }
    assert(0.1666667 === values(5))
    (6 to 7).foreach { i => assert(0.5 === values(i)) }
    assert(0.8333333 === values(8))
    assert(0.6666667 === values(9))
  }

  test("ROC") {
    val roc = columnSet.select("close") | ROC(3)
    val values = roc.firstDataColumn.values

    (0 to 2).foreach { i => assertResult(true)(values(i).isNaN) }

    Vector(0.08004271, 0.34830669, 0.22314355, 0.47957308, 0.25782911, 0.42744401, 0.13353139) zip (3 to 9) foreach {
      case (a, b) => assert(a === values(b))
    }
  }

  test("ROC future") {
    val roc = columnSet.select("close") | ROC(-3)
    val values = roc.firstDataColumn.values

    Vector(0.08004271, 0.34830669, 0.22314355, 0.47957308, 0.25782911, 0.42744401, 0.13353139) zip (0 to 6) foreach {
      case (a, b) => assert(a === values(b))
    }

    (7 to 9).foreach { i => assertResult(true)(values(i).isNaN) }
  }

  test("ROC twice") {
    val roc = columnSet.select("close") | ROC(3) | ROC(3)
    val values = roc.firstDataColumn.values

    (0 to 5).foreach { i => assertResult(true)(values(i).isNaN) }
    Vector(1.7903360, -0.3007864, 0.6500080, -1.2785597) zip (6 to 9) foreach { case (a, b) => assert(a === values(b)) }
  }

  test("RollingStatistics") {
    val rs = columnSet.select("close") | MeanVariance(4)
    val Seq(mean, variance) = rs.allDataColumns.map(_.values)

    (0 to 2).foreach { i => assertResult(true)(mean(i).isNaN) }
    Vector(1.225, 1.350, 1.425, 1.650, 1.875, 2.025, 2.250) zip (3 to 9) foreach {
      case (a, b) => assert(a === mean(b))
    }

    (0 to 2).foreach { i => assertResult(true)(variance(i).isNaN) }
    Vector(0.00250000, 0.05666667, 0.04916667, 0.11666667, 0.10916667, 0.12916667, 0.01666667) zip (3 to 9) foreach {
      case (a, b) => assert(a === variance(b))
    }
  }

  test("ROC then RollingStatistics") {
    val vol = columnSet.select("close") | ChainedCalculations(ROC(1), MeanVariance(4)) dataColumn "var"

    (0 to 3).foreach { i => assertResult(true)(vol(i).isNaN) }

    Vector(0.01601434, 0.02719665, 0.04298644, 0.04460519, 0.03672534, 0.02131303) zip (4 to 9) foreach {
      case (a, b) => assert(a === vol(b))
    }
  }

  test("Rogers-Satchell") {
    val vol = columnSet.select(ohlc: _*) | VolatilityRS(5, 5) dataColumn "rs"

    (0 to 3).foreach { i => assertResult(true)(vol(i).isNaN) }

    Vector(0.4503872, 0.4216357, 0.3295007, 0.2971905, 0.2673434, 0.2470792) zip (4 to 9) foreach {
      case (a, b) => assert(a === vol(b))
    }
  }

  test("Yang-Zhang") {
    val vol = columnSet.select(ohlc: _*) | VolatilityYZ(5, 5) dataColumn "yz"

    (0 to 4).foreach { i => assertResult(true)(vol(i).isNaN) }

    Vector(0.5946075, 0.6176882, 0.6140335, 0.5890155, 0.5257514) zip (5 to 9) foreach {
      case (a, b) => assert(a === vol(b))
    }
  }

  test("VolatilityCC") {
    val cc = columnSet.select("close") | VolatilityCC(1, 4) dataColumn "cc1"
    val vol = columnSet.select("close") | ROC(1) | MeanVariance(4) | function("var")(sqrt) dataColumn 0

    (cc.values zip vol.values).foreach { case (c, v) =>
      assert(c.isNaN && v.isNaN || c === v)
    }
  }

  test("EfficiencyRatio") {
    val er = columnSet | EfficiencyRatio(3) dataColumn "er"

    (0 to 2).foreach { i => assertResult(true)(er(i).isNaN) }

    Vector(1.0, 1.0, 0.4285714286, 0.6666666667, 0.5555555556, 1.0, 1.0) zip (3 to 9) foreach {
      case (a, b) => assert(a === er(b))
    }
  }

  test("RunAgg") {
    val hh = columnSet.select("high") | RunAgg(2, _.max) dataColumn "agg"
    val ll = columnSet.select("low") | RunAgg(3, _.min) dataColumn "agg"
    val sum = columnSet.select("close") | RunAgg(5, _.sum) dataColumn "agg"

    assertSeqEqual(hh.values, Vector(Double.NaN, 1.32, 1.33, 1.44, 1.85, 1.85, 2.22, 2.33, 2.44, 2.55))
    assertSeqEqual(ll.values, Vector(Double.NaN, Double.NaN, 0.90, 0.90, 1.05, 1.12, 1.33, 1.33, 1.99, 2.00))
    assertSeqEqual(sum.values, Vector(Double.NaN, Double.NaN, Double.NaN, Double.NaN, 6.6, 6.9, 7.8, 8.8, 9.8, 10.5))
  }

  test("RunAggShort") {
    val hh = columnSet.select("high") | RunAgg(999, _.max) dataColumn "agg"

    assertSeqEqual(hh.values, Vector.fill(10)(Double.NaN))
  }

  test("SigmaMoves") {
    val n = 5
    val result = columnSet | SigmaMoves(n)
    val ret = result.dataColumn("ret").values
    val upe = result.dataColumn("upe").values
    val dne = result.dataColumn("dne").values

    val close0 = (columnSet.select("close") | Lag(n)).firstDataColumn.values
    val hh = (columnSet.select("high") | RunAgg(n, _.max)).firstDataColumn.values
    val ll = (columnSet.select("low") | RunAgg(n, _.min)).firstDataColumn.values
    val sigma = (columnSet.select("sigma") | Lag(n)).firstDataColumn.values.map(_ * sqrt(n))
    val ret1 =  (columnSet.select("close") | ROC(n)).firstDataColumn.values zip sigma map { case (r, s) => r / s }
    val upe1 = hh zip close0 zip sigma map { case ((x, x0), s) => (log(x max x0) - log(x0)) / s }
    val dne1 = ll zip close0 zip sigma map { case ((x, x0), s) => (log(x min x0) - log(x0)) / s }

    assertSeqEqual(ret, ret1)
    assertSeqEqual(upe, upe1)
    assertSeqEqual(dne, dne1)
  }

  test("Future SigmaMoves") {
    val n = 5
    val result = columnSet | SigmaMoves(n)
    val result1 = columnSet | SigmaMoves(-n)

    val ret = result.dataColumn("ret").values
    val upe = result.dataColumn("upe").values
    val dne = result.dataColumn("dne").values

    val ret1 = (result1.select("ret") | Lag(n)).firstDataColumn.values
    val upe1 = (result1.select("upe") | Lag(n)).firstDataColumn.values
    val dne1 = (result1.select("dne") | Lag(n)).firstDataColumn.values

    assertSeqEqual(ret, ret1)
    assertSeqEqual(upe, upe1)
    assertSeqEqual(dne, dne1)
  }

  test("RunCov") {
    val n = 5
    val result = columnSet | RunCov("high", "low", n)
    val cov = result.dataColumn("cov").values
    val mean1 = result.dataColumn("mean1").values
    val mean2 = result.dataColumn("mean2").values
    val sma1 = (columnSet.select("high") | SMA(n)).firstDataColumn.values
    val sma2 = (columnSet.select("low") | SMA(n)).firstDataColumn.values

    // leading NaNs
    assert((0 until n - 1) forall(i => cov(i).isNaN))

    (high zip low).sliding(n).zipWithIndex foreach { case (pairs, i) =>
      assert(covariance(pairs) === cov(i + n - 1))
    }

    assertSeqEqual(sma1, mean1)
    assertSeqEqual(sma2, mean2)
  }

  test("RunSum") {
    val n = 5
    val sum = (columnSet.select("close") | RunSum(n)).firstDataColumn.values
    val sum1 = (columnSet.select("close") | RunAgg(n, _.sum)).firstDataColumn.values

    assertSeqEqual(sum, sum1)
  }

  test("RunLM") {
    val n = 7
    val result = columnSet | RunLM("high", "low", n)

    val triple = (high zip low).sliding(n).zipWithIndex map { case (pairs, i) =>

      val (x, y) = pairs.unzip
      val lm = LinearModel(x zip y)

      ((lm.alpha, lm.errAlpha), (lm.beta, lm.errBeta), (lm.r2, lm.errResidual))
    }

    val (a, b, r) = triple.toSeq.unzip3
    val (alpha, errAlpha) = a.unzip
    val (beta, errBeta) = b.unzip
    val (r2, errResidual) = r.unzip
    val leadingNaNs = Vector.fill(6)(Double.NaN)

    assertSeqEqual(leadingNaNs ++ alpha.toVector, result.dataColumn("alpha").values)
    assertSeqEqual(leadingNaNs ++ beta.toVector, result.dataColumn("beta").values)
    assertSeqEqual(leadingNaNs ++ r2.toVector, result.dataColumn("r2").values)

    assertSeqEqual(leadingNaNs ++ errAlpha.toVector, result.dataColumn("errAlpha").values)
    assertSeqEqual(leadingNaNs ++ errBeta.toVector, result.dataColumn("errBeta").values)
    assertSeqEqual(leadingNaNs ++ errResidual.toVector, result.dataColumn("errResidual").values)
  }

  test("TimeIndex") {
    val idx = (columnSet | TimeIndex).firstDataColumn.values

    assertSeqEqual(idx.indices map (_.toDouble), idx)
  }

  test("Time Trend t-score") {
    val n = 5
    val mom = (columnSet | TimeTrend(n)).firstDataColumn.values
    val logClose = close map log
    val idx = columnSet.timeColumn.indices

    // leading NaNs
    assert((0 until n - 1) forall(i => mom(i).isNaN))

    (logClose zip idx).sliding(n).zipWithIndex foreach { case (pairs, i) =>
      val (y, x) = pairs.unzip
      val lm = LinearModel(x map { i => i.toDouble } zip y)

      assert(lm.betaT === mom(i + n - 1))
    }
  }

  test("True Range") {
    val tr = (columnSet | TrueRange).firstDataColumn.values

    assertSeqEqual(tr, Vector(Double.NaN, 0.42, 0.28, 0.32, 0.55, 0.37, 0.72, 0.33, 0.33, 0.33))
  }
}
