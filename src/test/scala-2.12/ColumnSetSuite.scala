import java.time.LocalDate
import java.util.UUID

import com.leveragize.data._
import org.scalatest.FunSuite

/**
  * Created by peterho on 1/24/17.
  */
class ColumnSetSuite extends FunSuite {
  private val uuid = UUID.randomUUID()
  private val t = (1 to 10).map(LocalDate.of(2010, 1, _)).toVector

  val high = Vector(1.3, 1.3, 1.3, 1.4, 1.8, 1.6, 2.2, 2.3, 2.4, 2.5)
  val low = Vector(1.1, 1.1, 1.1, 1.2, 1.6, 1.4, 2.0, 2.1, 2.2, 2.3)
  val close = Vector(1.2, 1.2, 1.2, 1.3, 1.7, 1.5, 2.1, 2.2, 2.3, 2.4)

  val columnSet: ColumnSet[UUID, LocalDate, Double] with ColumnSetWithNew[UUID, LocalDate, Double] = {
    DefaultColumnSet(uuid, t, Seq(("high", high), ("low", low), ("close", close)))
  }

  test("correct columns") {
    assert(columnSet.columnNames === Seq("high", "low", "close"))
  }

  test("correct length") {
    assert(columnSet.timeColumn.length === 10)
  }

  test("correct data") {
    val dataOpt = columnSet.dataColumnOption("close")

    assert(dataOpt.isDefined)

    val data = dataOpt.get

    assertResult(1.3)(data(3))
    assertResult(true)(data(-1).isNaN)
    assertResult(0.0)(data(-1, 0.0))
  }

  test("getting non-existent column") {
    assertThrows[NoSuchElementException] {
      columnSet.dataColumn("abc")
    }
  }

  test("select columns") {
    val newColumnSet = columnSet.select("high")

    assertResult(Seq("high"))(newColumnSet.columnNames)
  }

  test("rename columns") {
    assertResult(Seq("high", "low", "close"))(columnSet.columnNames)

    val renamedColumnSet = columnSet.withRenamedColumns(Map("high" -> "a", "low" -> "b"))

    assertResult(Seq("a", "b", "close"))(renamedColumnSet.columnNames)

    renamedColumnSet.dataColumns(Seq("a", "b")) match {
      case Seq(a, b) =>
        assertResult(2.5)(a.values(9))
        assertResult(2.3)(b.values(9))
    }
  }

  test("slice") {
    val subset = columnSet.withIndexRange(3)

    assertResult(7)(subset.length)

    assert(subset.dataColumn("high").values === high.slice(3, 10))
    assert(subset.dataColumn("low").values === low.slice(3, 10))
    assert(subset.dataColumn("close").values === close.slice(3, 10))

    assert(subset.dataColumn("high").slice(1, 5).values === high.slice(4, 8))
    assert(subset.dataColumn("low").slice(1, 5).values === low.slice(4, 8))
    assert(subset.dataColumn("close").slice(1, 5).values === close.slice(4, 8))
  }
}
